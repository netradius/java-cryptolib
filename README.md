# NetRadius Java Crypto Library

This library provides useful components to aid in common cryptographic tasks.

## Downloads
Releases of this library are published  on [Maven Central](https://search.maven.org/).

 * Release Repository - https://repo.maven.apache.org/maven2/com/netradius/cryptolib/
 * Staging Repository - https://oss.sonatype.org/content/groups/staging/com/netradius/cryptolib/
 * Snapshot Repository - https://oss.sonatype.org/content/repositories/snapshots/com/netradius/cryptolib/

## Java Compatibility

This project tests on Java LTS releases only. At present this library targets the Java 8 runtime and
is tested to work on Java 11.

## Bugs & Enhancement Requests

Please file any bugs or enhancements at https://bitbucket.org/netradius/java-cryptolib/issues

## License
This project is licensed under the BSD 3-Clause License. See [LICENSE.txt](https://bitbucket.org/netradius/java-cryptolib/src/master/LICENSE.txt)

## Local Development

To install a new jar file locally without running unit tests and skipping the GPG signing 
process, execute the following:

```bash
 ./mvnw -Dgpg.skip=true -DskipTests=true install
```
