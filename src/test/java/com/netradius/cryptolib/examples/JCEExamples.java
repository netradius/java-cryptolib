/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.examples;

import com.netradius.commons.lang.ArrayHelper;
import com.netradius.cryptolib.BCHelper;
import com.netradius.cryptolib.CryptoException;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.util.Random;

import static java.lang.System.out;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class JCEExamples {

  @BeforeClass
  public static void init() {
    Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
  }

  private static final SecureRandom SECURE_RANDOM = new SecureRandom();
  private static final String PLAIN_TEXT = "No one can build his security upon the nobleness of another person.";
  private static final byte[] PLAIN_TEXT_BYTES = PLAIN_TEXT.getBytes(StandardCharsets.UTF_8);
  private static final String ADDITIONAL_AUTH_DATA = "some such or some thing to auth with";
  private static final byte[] ADDITIONAL_AUTH_DATA_BYTES = ADDITIONAL_AUTH_DATA.getBytes(StandardCharsets.UTF_8);
  private static final String PASSWORD = "Benjamin Franklin";

  @Test
  public void AES_CBC() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
      IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {

    out.println("Example: AES/CBC/PKCS5Padding");

    // Generate key
    KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
    keyGenerator.init(128, SECURE_RANDOM); // explicitly specify a 128 bit key
    SecretKey key = keyGenerator.generateKey();

    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding"); // JCE is dumb and PKCS5Padding really means PKCS7Padding

    // Generate required initialization vector
    byte[] iv = new byte[cipher.getBlockSize()]; // this will be 16 bytes for AES
    SECURE_RANDOM.nextBytes(iv);
    IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

    // Encrypt
    cipher.init(Cipher.ENCRYPT_MODE, key, ivParameterSpec, SECURE_RANDOM);
    out.println(String.format("Block Size: %d", cipher.getBlockSize()));
    byte[] encrypted = cipher.doFinal(PLAIN_TEXT_BYTES);
    out.println(String.format("Encrypted bytes: %d", encrypted.length));

    // Decrypt
    cipher.init(Cipher.DECRYPT_MODE, key, ivParameterSpec, SECURE_RANDOM);
    String decrypted = new String(cipher.doFinal(encrypted), StandardCharsets.UTF_8);

    // Validate
    assertEquals(decrypted, PLAIN_TEXT);
    out.println();
  }

  @Test
  public void AES_GCME() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
      IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {

    out.println("Example: AES/GCM/NoPadding");

    // Generate key
    KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
    keyGenerator.init(128, SECURE_RANDOM); // explicitly specify a 128 bit key
    SecretKey key = keyGenerator.generateKey();

    Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding"); // GCM doesn't require padding

    // Generate required initialization vector which is essentially a nonce
    byte[] iv = new byte[12]; // a 12 byte iv is recommended to avoid additional calculations
    SECURE_RANDOM.nextBytes(iv);
    GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(128, iv); // 128 bit tag length is recommended

    // Encrypt
    cipher.init(Cipher.ENCRYPT_MODE, key, gcmParameterSpec, SECURE_RANDOM);
    out.println(String.format("Block Size: %d", cipher.getBlockSize()));
    byte[] encrypted = cipher.doFinal(PLAIN_TEXT_BYTES);
    out.println(String.format("Encrypted bytes: %d", encrypted.length));

    // Decrypt
    cipher.init(Cipher.DECRYPT_MODE, key, gcmParameterSpec, SECURE_RANDOM);
    String decrypted = new String(cipher.doFinal(encrypted), StandardCharsets.UTF_8);

    // Validate
    assertEquals(decrypted, PLAIN_TEXT);
    out.println();
  }

  @Test
  public void AES_GCM_MULTIPART() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
      IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {

    byte[] data = new byte[1024 * 1024 * 100]; // 100MB
    new Random().nextBytes(data);

    out.println("Example: AES/GCM/NoPadding");

    // Generate key
    KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
    keyGenerator.init(128, SECURE_RANDOM); // explicitly specify a 128 bit key
    SecretKey key = keyGenerator.generateKey();

    // We use the BC provider because decryption will work in chunks
    Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding", "BC"); // GCM doesn't require padding

    // Generate required initialization vector which is essentially a nonce
    byte[] iv = new byte[12]; // a 12 byte iv is recommended to avoid additional calculations
    SECURE_RANDOM.nextBytes(iv);
    GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(128, iv); // 128 bit tag length is recommended

    // Encrypt
    cipher.init(Cipher.ENCRYPT_MODE, key, gcmParameterSpec, SECURE_RANDOM);
    out.println(String.format("Block Size: %d", cipher.getBlockSize()));
    byte[] eu = cipher.update(data);
    byte[] fu = cipher.doFinal();
    byte[] encrypted = ArrayHelper.concat(eu, fu);
    out.println(String.format("Encrypted bytes: %d", encrypted.length));

    // Decrypt
    cipher.init(Cipher.DECRYPT_MODE, key, gcmParameterSpec, SECURE_RANDOM);
    eu = cipher.update(encrypted);
    assertTrue(eu.length > 0);
    fu = cipher.doFinal();
    byte[] decrypted = ArrayHelper.concat(eu, fu);

    // Validate
    assertTrue(ArrayHelper.equals(data, decrypted));
    out.println();
  }

  @Test(expected = AEADBadTagException.class)
  public void AES_GCM_MULTIPART_ERROR() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
      IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {

    byte[] data = new byte[1024 * 1024]; // 1MB
    new Random().nextBytes(data);

    out.println("Example: AES/GCM/NoPadding");

    // Generate key
    KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
    keyGenerator.init(128, SECURE_RANDOM); // explicitly specify a 128 bit key
    SecretKey key = keyGenerator.generateKey();

    // We use the BC provider because decryption will work in chunks
    Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding", "BC"); // GCM doesn't require padding

    // Generate required initialization vector which is essentially a nonce
    byte[] iv = new byte[12]; // a 12 byte iv is recommended to avoid additional calculations
    SECURE_RANDOM.nextBytes(iv);
    GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(128, iv); // 128 bit tag length is recommended

    // Encrypt
    cipher.init(Cipher.ENCRYPT_MODE, key, gcmParameterSpec, SECURE_RANDOM);
    out.println(String.format("Block Size: %d", cipher.getBlockSize()));
    byte[] eu = cipher.update(data);
    byte[] fu = cipher.doFinal();
    byte[] encrypted = ArrayHelper.concat(eu, fu);
    out.println(String.format("Encrypted bytes: %d", encrypted.length));

    encrypted[data.length-1] = 1;

    // Decrypt
    cipher.init(Cipher.DECRYPT_MODE, key, gcmParameterSpec, SECURE_RANDOM);
    eu = cipher.update(encrypted);
    assertTrue(eu.length > 0);
    fu = cipher.doFinal();
    byte[] decrypted = ArrayHelper.concat(eu, fu);

    // Validate
    assertTrue(ArrayHelper.equals(data, decrypted));
    out.println();
  }

  @Test
  public void AES_GCM_AAD() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
      IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {

    out.println("Example: AES/GCM/NoPadding with AAD");

    // Generate key
    KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
    keyGenerator.init(128, SECURE_RANDOM); // explicitly specify a 128 bit key
    SecretKey key = keyGenerator.generateKey();

    Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding"); // GCM doesn't require padding

    // Generate required initialization vector which is essentially a nonce
    byte[] iv = new byte[12]; // a 12 byte iv is recommended to avoid additional calculations
    SECURE_RANDOM.nextBytes(iv);
    GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(128, iv); // 128 bit tag length is recommended

    // Encrypt
    cipher.init(Cipher.ENCRYPT_MODE, key, gcmParameterSpec, SECURE_RANDOM);
    out.println(String.format("Block Size: %d", cipher.getBlockSize()));
    cipher.updateAAD(ADDITIONAL_AUTH_DATA_BYTES); // optional additional auth data
    byte[] encrypted = cipher.doFinal(PLAIN_TEXT_BYTES);
    out.println(String.format("Encrypted bytes: %d", encrypted.length));

    // Decrypt
    cipher.init(Cipher.DECRYPT_MODE, key, gcmParameterSpec, SECURE_RANDOM);
    cipher.updateAAD(ADDITIONAL_AUTH_DATA_BYTES); // optional additional auth data
    String decrypted = new String(cipher.doFinal(encrypted), StandardCharsets.UTF_8);

    // Validate
    assertEquals(decrypted, PLAIN_TEXT);
    out.println();
  }

  @Test
  public void RSA() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
      IllegalBlockSizeException, BadPaddingException, CryptoException {

    // See https://www.bouncycastle.org/specifications.html
    BCHelper.register(); // register bouncy castle for use with this cipher
    out.println("Example: RSA/NONE/OAEPWithSHA3-512AndMGF1Padding");

    // Generate key pair
    KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
    keyPairGenerator.initialize(2048, SECURE_RANDOM); // explicitly set 2048 bit key size
    KeyPair keyPair = keyPairGenerator.generateKeyPair();

    Cipher cipher = Cipher.getInstance("RSA/NONE/OAEPWithSHA3-512AndMGF1Padding");

    // Encrypt
    cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic(), SECURE_RANDOM);
    out.println(String.format("Block Size: %d", cipher.getBlockSize()));
    byte[] encrypted = cipher.doFinal(PLAIN_TEXT_BYTES);
    out.println(String.format("Encrypted bytes: %d", encrypted.length));

    // Decrypt
    cipher.init(Cipher.DECRYPT_MODE, keyPair.getPrivate(), SECURE_RANDOM);
    String decrypted = new String(cipher.doFinal(encrypted), StandardCharsets.UTF_8);

    // Validate
    assertEquals(decrypted, PLAIN_TEXT);
    out.println();
  }

  @Test
  public void PBE() throws NoSuchAlgorithmException, InvalidKeySpecException,  NoSuchPaddingException,
      InvalidKeyException, IllegalBlockSizeException, BadPaddingException, CryptoException,
      InvalidAlgorithmParameterException {

    // See https://www.bouncycastle.org/specifications.html
    BCHelper.register(); // register bouncy castle for use with this cipher
    out.println("Example: PBEWithSHAAndTwofish-CBC");

    // Generate salt - 160 bits matches Bouncy Castle Uber Keystore
    byte[] salt = new byte[20];
    SECURE_RANDOM.nextBytes(salt);

    // Obtain iterations - between 1024 and 2047 matches Bouncy Castle Uber Keystore
    int iterations = SECURE_RANDOM.nextInt(2047 - 1024) + 1024;
    out.println(String.format("Iterations: %d", iterations));

    // Generate key
    PBEKeySpec pbeKeySpec = new PBEKeySpec(PASSWORD.toCharArray(), salt, iterations);
    SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBEWithSHAAndTwofish-CBC");
    Key key = secretKeyFactory.generateSecret(pbeKeySpec);

    Cipher cipher = Cipher.getInstance("PBEWithSHAAndTwofish-CBC");

    // Generate required initialization vector
    byte[] iv = new byte[cipher.getBlockSize()]; // this will be 16 bytes for AES
    SECURE_RANDOM.nextBytes(iv);
    IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

    // Encrypt
    cipher.init(Cipher.ENCRYPT_MODE, key, ivParameterSpec, SECURE_RANDOM);
    out.println(String.format("Block Size: %d", cipher.getBlockSize()));
    byte[] encrypted = cipher.doFinal(PLAIN_TEXT_BYTES);
    out.println(String.format("Encrypted bytes: %d", encrypted.length));


    // Re-generate key to demonstrate PBE
    pbeKeySpec = new PBEKeySpec(PASSWORD.toCharArray(), salt, iterations);
    secretKeyFactory = SecretKeyFactory.getInstance("PBEWithSHAAndTwofish-CBC");
    key = secretKeyFactory.generateSecret(pbeKeySpec);

    // Decrypt
    cipher.init(Cipher.DECRYPT_MODE, key, ivParameterSpec, SECURE_RANDOM);
    String decrypted = new String(cipher.doFinal(encrypted), StandardCharsets.UTF_8);

    // Validate
    assertEquals(decrypted, PLAIN_TEXT);
    out.println();
  }

  @Test
  public void PBEHmac() throws NoSuchAlgorithmException, InvalidKeySpecException,  NoSuchPaddingException,
      InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
      InvalidAlgorithmParameterException {

    // Generate salt
    byte[] salt = new byte[64]; // 512 / 8
    SECURE_RANDOM.nextBytes(salt);

    int iterations = 200000; // selected a large number

    // Generate key
    PBEKeySpec pbeKeySpec = new PBEKeySpec(PASSWORD.toCharArray(), salt, iterations);
    SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBEWithHmacSHA512AndAES_128");
    Key key = secretKeyFactory.generateSecret(pbeKeySpec);

    Cipher cipher = Cipher.getInstance("PBEWithHmacSHA512AndAES_128");

    // Generate required initialization vector
    byte[] iv = new byte[cipher.getBlockSize()]; // this will be 16 bytes for AES
    SECURE_RANDOM.nextBytes(iv);
    IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

    PBEParameterSpec pbeParameterSpec = new PBEParameterSpec(salt, iterations, ivParameterSpec);

    // Encrypt
    cipher.init(Cipher.ENCRYPT_MODE, key, pbeParameterSpec, SECURE_RANDOM);
    out.println(String.format("Block Size: %d", cipher.getBlockSize()));
    byte[] encrypted = cipher.doFinal(PLAIN_TEXT_BYTES);
    out.println(String.format("Encrypted bytes: %d", encrypted.length));

    // Re-generate key to demonstrate PBE
    pbeKeySpec = new PBEKeySpec(PASSWORD.toCharArray(), salt, iterations);
    secretKeyFactory = SecretKeyFactory.getInstance("PBEWithHmacSHA512AndAES_128");
    key = secretKeyFactory.generateSecret(pbeKeySpec);

    // Decrypt
    cipher.init(Cipher.DECRYPT_MODE, key, pbeParameterSpec, SECURE_RANDOM);
    String decrypted = new String(cipher.doFinal(encrypted), StandardCharsets.UTF_8);

    // Validate
    assertEquals(decrypted, PLAIN_TEXT);
    out.println();
  }
}
