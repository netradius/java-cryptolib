/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.examples;

import com.netradius.cryptolib.CryptoException;
import com.netradius.cryptolib.provider.AESProvider;
import com.netradius.cryptolib.provider.PBEProvider;
import com.netradius.cryptolib.provider.RSAProvider;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

import static java.lang.System.out;
import static org.junit.Assert.assertEquals;

public class ProviderExamples {

  private static final String PLAIN_TEXT = "No one can build his security upon the nobleness of another person.";
  private static final byte[] PLAIN_TEXT_BYTES = PLAIN_TEXT.getBytes(StandardCharsets.UTF_8);
  private static final String ADDITIONAL_AUTH_DATA = "some such or some thing to auth with";
  private static final byte[] ADDITIONAL_AUTH_DATA_BYTES = ADDITIONAL_AUTH_DATA.getBytes(StandardCharsets.UTF_8);
  private static final String PASSWORD = "Benjamin Franklin";

  @Test
  public void AES_CBC() throws CryptoException {

    out.println("Example: AES/CBC/PKCS5Padding");

    // Generate key
    AESProvider.CBC cbc = AESProvider.aes128().cbc();

    // Encrypt
    byte[] encrypted = cbc.encrypt(PLAIN_TEXT_BYTES);
    out.println(String.format("Encrypted bytes: %d", encrypted.length));

    // Decrypt
    String decrypted = new String(cbc.decrypt(encrypted), StandardCharsets.UTF_8);

    // Validate
    assertEquals(decrypted, PLAIN_TEXT);
    out.println();
  }

  @Test
  public void AES_GCM() throws CryptoException {

    out.println("Example: AES/GCM/NoPadding");

    // Generate key
    AESProvider.GCM gcm = AESProvider.aes128().gcm128();

    // Encrypt
    byte[] encrypted = gcm.encrypt(PLAIN_TEXT_BYTES);
    out.println(String.format("Encrypted bytes: %d", encrypted.length));

    // Decrypt
    String decrypted = new String(gcm.decrypt(encrypted), StandardCharsets.UTF_8);

    // Validate
    assertEquals(decrypted, PLAIN_TEXT);
    out.println();
  }

  @Test
  public void AES_GCM_AAD() throws CryptoException {

    out.println("Example: AES/GCM/NoPadding");

    // Generate key
    AESProvider.GCM gcm = AESProvider.aes128().gcm128();

    // Encrypt
    byte[] encrypted = gcm.encrypt(PLAIN_TEXT_BYTES, ADDITIONAL_AUTH_DATA_BYTES);
    out.println(String.format("Encrypted bytes: %d", encrypted.length));

    // Decrypt
    String decrypted = new String(gcm.decrypt(encrypted, ADDITIONAL_AUTH_DATA_BYTES), StandardCharsets.UTF_8);

    // Validate
    assertEquals(decrypted, PLAIN_TEXT);
    out.println();
  }

  @Test
  public void RSA() throws CryptoException {

    out.println("Example: RSA/NONE/OAEPWithSHA3-512AndMGF1Padding");

    // Generate key pair
    RSAProvider.RSA rsa = RSAProvider.rsa2048().rsa();

    // Encrypt
    byte[] encrypted = rsa.encrypt(PLAIN_TEXT_BYTES);
    out.println(String.format("Encrypted bytes: %d", encrypted.length));

    // Decrypt
    String decrypted = new String(rsa.decrypt(encrypted), StandardCharsets.UTF_8);

    // Validate
    assertEquals(decrypted, PLAIN_TEXT);
    out.println();
  }

  @Test
  public void PBE() throws CryptoException {

    out.println("Example: PBEWithSHAAndTwofish-CBC");

    // Generate key, salt and iterations
    PBEProvider.PBE pbe = PBEProvider.pbe(PASSWORD.toCharArray())
        .sha().twofish();

    // Encrypt
    byte[] encrypted = pbe.encrypt(PLAIN_TEXT_BYTES);
    out.println(String.format("Encrypted bytes: %d", encrypted.length));

    // Reset to show generated key is identical
    pbe = PBEProvider.pbe(PASSWORD.toCharArray())
        .salt(pbe.salt())
        .iterations(pbe.iterations())
        .sha().twofish();

    // Decrypt
    String decrypted = new String(pbe.decrypt(encrypted), StandardCharsets.UTF_8);

    // Validate
    assertEquals(decrypted, PLAIN_TEXT);
    out.println();
  }

}
