/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.provider;

import com.netradius.commons.digest.DigestHelper;
import com.netradius.commons.util.RandomHelper;
import com.netradius.cryptolib.CryptoException;
import org.junit.Test;

import java.io.IOException;
import java.nio.channels.ByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.SecureRandom;

import static java.lang.System.out;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;

public class AESProviderTest {

  private static final String PLAIN_TEXT = "No one can build his security upon the nobleness of another person.";
  private static final byte[] PLAIN_TEXT_BYTES = PLAIN_TEXT.getBytes(UTF_8);
  private static final String ADDITIONAL_AUTH_DATA = "some such or some thing to auth with";
  private static final byte[] ADDITIONAL_AUTH_DATA_BYTES = ADDITIONAL_AUTH_DATA.getBytes(UTF_8);

  @Test
  public void testCbc() throws CryptoException {
    AESProvider.CBC cbc = AESProvider.aes128().cbc();
    assertEquals(PLAIN_TEXT, new String(cbc.decrypt(cbc.encrypt(PLAIN_TEXT_BYTES)), UTF_8));

    cbc = AESProvider.aes192().cbc();
    assertEquals(PLAIN_TEXT, new String(cbc.decrypt(cbc.encrypt(PLAIN_TEXT_BYTES)), UTF_8));

    cbc = AESProvider.aes256().random(new SecureRandom()).cbc();
    assertEquals(PLAIN_TEXT, new String(cbc.decrypt(cbc.encrypt(PLAIN_TEXT_BYTES)), UTF_8));

    cbc = AESProvider.aes(cbc.key()).cbc();
    assertEquals(PLAIN_TEXT, new String(cbc.decrypt(cbc.encrypt(PLAIN_TEXT_BYTES)), UTF_8));

    cbc = AESProvider.aes(cbc.keyBytes()).cbc();
    assertEquals(PLAIN_TEXT, new String(cbc.decrypt(cbc.encrypt(PLAIN_TEXT_BYTES)), UTF_8));

    cbc = AESProvider.aes(cbc.keyBase64()).cbc();
    assertEquals(PLAIN_TEXT, new String(cbc.decrypt(cbc.encrypt(PLAIN_TEXT_BYTES)), UTF_8));
  }

  @Test
  public void testGcm() throws CryptoException {
    AESProvider.GCM gcm = AESProvider.aes128().gcm96();
    assertEquals(PLAIN_TEXT, new String(gcm.decrypt(gcm.encrypt(PLAIN_TEXT_BYTES)), UTF_8));
    assertEquals(PLAIN_TEXT, new String(gcm.decrypt(gcm.encrypt(PLAIN_TEXT_BYTES, ADDITIONAL_AUTH_DATA_BYTES),
        ADDITIONAL_AUTH_DATA_BYTES), UTF_8));

    gcm = AESProvider.aes128().gcm104();
    assertEquals(PLAIN_TEXT, new String(gcm.decrypt(gcm.encrypt(PLAIN_TEXT_BYTES)), UTF_8));
    assertEquals(PLAIN_TEXT, new String(gcm.decrypt(gcm.encrypt(PLAIN_TEXT_BYTES, ADDITIONAL_AUTH_DATA_BYTES),
        ADDITIONAL_AUTH_DATA_BYTES), UTF_8));

    gcm = AESProvider.aes128().gcm112();
    assertEquals(PLAIN_TEXT, new String(gcm.decrypt(gcm.encrypt(PLAIN_TEXT_BYTES)), UTF_8));
    assertEquals(PLAIN_TEXT, new String(gcm.decrypt(gcm.encrypt(PLAIN_TEXT_BYTES, ADDITIONAL_AUTH_DATA_BYTES),
        ADDITIONAL_AUTH_DATA_BYTES), UTF_8));

    gcm = AESProvider.aes128().gcm120();
    assertEquals(PLAIN_TEXT, new String(gcm.decrypt(gcm.encrypt(PLAIN_TEXT_BYTES)), UTF_8));
    assertEquals(PLAIN_TEXT, new String(gcm.decrypt(gcm.encrypt(PLAIN_TEXT_BYTES, ADDITIONAL_AUTH_DATA_BYTES),
        ADDITIONAL_AUTH_DATA_BYTES), UTF_8));

    gcm = AESProvider.aes128().random(new SecureRandom()).gcm128();
    assertEquals(PLAIN_TEXT, new String(gcm.decrypt(gcm.encrypt(PLAIN_TEXT_BYTES)), UTF_8));
    assertEquals(PLAIN_TEXT, new String(gcm.decrypt(gcm.encrypt(PLAIN_TEXT_BYTES, ADDITIONAL_AUTH_DATA_BYTES),
        ADDITIONAL_AUTH_DATA_BYTES), UTF_8));

    gcm = AESProvider.aes(gcm.key()).gcm104();
    assertEquals(PLAIN_TEXT, new String(gcm.decrypt(gcm.encrypt(PLAIN_TEXT_BYTES)), UTF_8));
    assertEquals(PLAIN_TEXT, new String(gcm.decrypt(gcm.encrypt(PLAIN_TEXT_BYTES, ADDITIONAL_AUTH_DATA_BYTES),
        ADDITIONAL_AUTH_DATA_BYTES), UTF_8));

    gcm = AESProvider.aes(gcm.keyBytes()).gcm104();
    assertEquals(PLAIN_TEXT, new String(gcm.decrypt(gcm.encrypt(PLAIN_TEXT_BYTES)), UTF_8));
    assertEquals(PLAIN_TEXT, new String(gcm.decrypt(gcm.encrypt(PLAIN_TEXT_BYTES, ADDITIONAL_AUTH_DATA_BYTES),
        ADDITIONAL_AUTH_DATA_BYTES), UTF_8));

    gcm = AESProvider.aes(gcm.keyBase64()).gcm104();
    assertEquals(PLAIN_TEXT, new String(gcm.decrypt(gcm.encrypt(PLAIN_TEXT_BYTES)), UTF_8));
    assertEquals(PLAIN_TEXT, new String(gcm.decrypt(gcm.encrypt(PLAIN_TEXT_BYTES, ADDITIONAL_AUTH_DATA_BYTES),
        ADDITIONAL_AUTH_DATA_BYTES), UTF_8));

    AESProvider.GCM gcm1 = AESProvider.aes128().gcm128();
    AESProvider.GCM gcm2 = AESProvider.aes(gcm1.keyBytes()).gcm128();
    assertEquals(PLAIN_TEXT, new String(gcm2.decrypt(gcm1.encrypt(PLAIN_TEXT_BYTES)), UTF_8));
    assertEquals(PLAIN_TEXT, new String(gcm2.decrypt(gcm1.encrypt(PLAIN_TEXT_BYTES, ADDITIONAL_AUTH_DATA_BYTES),
        ADDITIONAL_AUTH_DATA_BYTES), UTF_8));
  }

  @Test(expected = CryptoException.class)
  public void testGcmTagFailure() throws CryptoException {
    AESProvider.GCM gcm = AESProvider.aes128().gcm128();
    byte[] encrypted = gcm.encrypt(PLAIN_TEXT_BYTES, ADDITIONAL_AUTH_DATA_BYTES);
    encrypted[encrypted.length - 1]++;
    gcm.decrypt(encrypted, ADDITIONAL_AUTH_DATA_BYTES);
  }

  @Test
  public void gigabyteTest() throws IOException, CryptoException {

    out.println("Generating random data file");
    Path o = Paths.get(System.getProperty("java.io.tmpdir"), "cryptolib.gigabyte");
    RandomHelper.rfile(o.toFile(), 1024 * 1024 * 1024);
    out.println(String.format("Original:  %d bytes", o.toFile().length()));

    AESProvider.GCM gcm = AESProvider.aes128().bcProvider().gcm128();

    out.println("Encrypting data file");
    Path e = Paths.get(System.getProperty("java.io.tmpdir"), "cryptolib.encrypted");
    e.toFile().deleteOnExit();
    try (ByteChannel in = Files.newByteChannel(o, StandardOpenOption.READ);
         ByteChannel out = Files.newByteChannel(e, StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW)) {
      gcm.encrypt(in, out);
    }
    out.println(String.format("Encrypted: %d bytes", e.toFile().length()));
    assertEquals(gcm.ciphertextLength((int)o.toFile().length()), e.toFile().length());

    out.println("Decrypting data file");
    Path d = Paths.get(System.getProperty("java.io.tmpdir"), "cryptolib.decrypted");
    d.toFile().deleteOnExit();
    try (ByteChannel in = Files.newByteChannel(e, StandardOpenOption.READ);
         ByteChannel out = Files.newByteChannel(d, StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW)) {
        gcm.decrypt(in, out);
    }
    out.println(String.format("Decrypted: %d bytes", d.toFile().length()));
    // plaintextLength + IV (12 bytes) + tag (16 bytes)
    assertEquals(o.toFile().length() + 12 + 16, e.toFile().length());

    out.println("Checking digests");
    assertEquals(DigestHelper.sha1(o.toFile()), DigestHelper.sha1(d.toFile()));
  }
}
