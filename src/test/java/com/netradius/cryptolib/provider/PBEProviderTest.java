/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.provider;

import com.netradius.cryptolib.CryptoException;
import org.junit.Test;

import java.security.SecureRandom;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;

public class PBEProviderTest {

  private static final String PLAIN_TEXT = "No one can build his security upon the nobleness of another person.";
  private static final byte[] PLAIN_TEXT_BYTES = PLAIN_TEXT.getBytes(UTF_8);
  private static final String PASSWORD = "Benjamin Franklin";
  private static final char[] PASSWORD_CHARS = PASSWORD.toCharArray();

  @Test
  public void testSha() throws CryptoException {
    PBEProvider.PBE pbe = PBEProvider.pbe(PASSWORD_CHARS).sha().twofish();
    assertEquals(PLAIN_TEXT, new String(pbe.decrypt(pbe.encrypt(PLAIN_TEXT_BYTES)), UTF_8));

    pbe = PBEProvider.pbe(PASSWORD_CHARS).sha().aes128();
    assertEquals(PLAIN_TEXT, new String(pbe.decrypt(pbe.encrypt(PLAIN_TEXT_BYTES)), UTF_8));

    pbe = PBEProvider.pbe(PASSWORD_CHARS).sha().aes192();
    assertEquals(PLAIN_TEXT, new String(pbe.decrypt(pbe.encrypt(PLAIN_TEXT_BYTES)), UTF_8));

    pbe = PBEProvider.pbe(PASSWORD_CHARS).random(new SecureRandom()).sha().aes256();
    assertEquals(PLAIN_TEXT, new String(pbe.decrypt(pbe.encrypt(PLAIN_TEXT_BYTES)), UTF_8));

    pbe = PBEProvider.pbe(PASSWORD_CHARS).iterations(pbe.iterations()).salt(pbe.salt()).sha().aes256();
    assertEquals(PLAIN_TEXT, new String(pbe.decrypt(pbe.encrypt(PLAIN_TEXT_BYTES)), UTF_8));
  }

  @Test
  public void testHmacSha1() throws CryptoException {
    PBEProvider.PBE pbe = PBEProvider.pbe(PASSWORD_CHARS).hmacSha1().aes128();
    assertEquals(PLAIN_TEXT, new String(pbe.decrypt(pbe.encrypt(PLAIN_TEXT_BYTES)), UTF_8));

    pbe = PBEProvider.pbe(PASSWORD_CHARS).random(new SecureRandom()).hmacSha1().aes256();
    assertEquals(PLAIN_TEXT, new String(pbe.decrypt(pbe.encrypt(PLAIN_TEXT_BYTES)), UTF_8));

    pbe = PBEProvider.pbe(PASSWORD_CHARS).iterations(pbe.iterations()).salt(pbe.salt()).hmacSha1().aes256();
    assertEquals(PLAIN_TEXT, new String(pbe.decrypt(pbe.encrypt(PLAIN_TEXT_BYTES)), UTF_8));
  }

}
