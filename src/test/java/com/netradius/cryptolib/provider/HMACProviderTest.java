/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.provider;

import com.netradius.commons.lang.ArrayHelper;
import com.netradius.cryptolib.CryptoException;
import org.junit.Test;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HMACProviderTest {

  private static final String PLAIN_TEXT = "Privacy is not for the passive.";
  private static final byte[] PLAIN_TEXT_BYTES = PLAIN_TEXT.getBytes(UTF_8);

  @Test
  public void testSha1() throws CryptoException {
    HMACProvider.HMAC hmac1 = HMACProvider.sha1().hmac();
    assertEquals("HmacSHA1", hmac1.algorithm());
    assertEquals(160, hmac1.hashLength());
    assertEquals(20, hmac1.hashLengthBytes());
    byte[] mac1 = hmac1.encode(PLAIN_TEXT_BYTES);
    assertEquals(hmac1.hashLengthBytes(), mac1.length);

    HMACProvider.HMAC hmac2 = HMACProvider.sha1().key(hmac1.key()).hmac();
    byte[] mac2 = hmac2.encode(PLAIN_TEXT_BYTES);
    assertTrue(ArrayHelper.equals(mac1, mac2));
  }

  @Test
  public void testSha256() throws CryptoException {
    HMACProvider.HMAC hmac1 = HMACProvider.sha256().hmac();
    assertEquals("HmacSHA256", hmac1.algorithm());
    assertEquals(256, hmac1.hashLength());
    assertEquals(32, hmac1.hashLengthBytes());
    byte[] mac1 = hmac1.encode(PLAIN_TEXT_BYTES);
    assertEquals(hmac1.hashLengthBytes(), mac1.length);

    HMACProvider.HMAC hmac2 = HMACProvider.sha256().key(hmac1.key()).hmac();
    byte[] mac2 = hmac2.encode(PLAIN_TEXT_BYTES);
    assertTrue(ArrayHelper.equals(mac1, mac2));
  }

  @Test
  public void testSha512() throws CryptoException {
    HMACProvider.HMAC hmac1 = HMACProvider.sha512().hmac();
    assertEquals("HmacSHA512", hmac1.algorithm());
    assertEquals(512, hmac1.hashLength());
    assertEquals(64, hmac1.hashLengthBytes());
    byte[] mac1 = hmac1.encode(PLAIN_TEXT_BYTES);
    assertEquals(hmac1.hashLengthBytes(), mac1.length);

    HMACProvider.HMAC hmac2 = HMACProvider.sha512().key(hmac1.key()).hmac();
    byte[] mac2 = hmac2.encode(PLAIN_TEXT_BYTES);
    assertTrue(ArrayHelper.equals(mac1, mac2));
  }

}
