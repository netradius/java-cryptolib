/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.pgp;

import com.netradius.commons.io.IOHelper;
import com.netradius.cryptolib.CryptoException;
import com.netradius.cryptolib.pgp.PGPHelper;
import org.bouncycastle.openpgp.PGPException;
import org.junit.Test;

import java.io.*;
import java.nio.charset.Charset;

import static java.lang.System.out;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


/**
 * Unit tests for PGPHelper.
 *
 * @author Erik R. Jensen
 */
public class PGPHelperTest {

	private static final Charset UTF8 = Charset.forName("UTF-8");

	private File generateTestFile(String filename, String msg) throws IOException {
		File file = new File(System.getProperty("java.io.tmpdir"), filename);
		try (FileOutputStream fos = new FileOutputStream(file);
			 OutputStreamWriter osw = new OutputStreamWriter(fos, UTF8);
			 PrintWriter writer = new PrintWriter(osw)) {
			writer.write(msg);
		}
		out.println("Created test file " + file.getAbsolutePath());
		return file;
	}

	private String readContents(File file) throws IOException {
		try (FileInputStream fis = new FileInputStream(file);
			 BufferedInputStream bis = new BufferedInputStream(fis)) {
			return IOHelper.readString(bis);
		}
	}

	@Test
	public void testSignature() throws IOException, CryptoException {
		String msg = "This is a signature test!";

		// Generate keys
		ByteArrayOutputStream pubout = new ByteArrayOutputStream();
		ByteArrayOutputStream secout = new ByteArrayOutputStream();
		PGPHelper.generate("test", "test", true, pubout, secout);
		byte[] pubKey = pubout.toByteArray();
		byte[] secKey = secout.toByteArray();

		// Generate test file
		File file = generateTestFile("PGPHelperSignatureTest.txt", msg);

		// Sign test file
		File signedFile = new File(System.getProperty("java.io.tmpdir"), "PGPHelperSignatureTest.signed");
		try (FileOutputStream out = new FileOutputStream(signedFile)) {
			PGPHelper.sign(file, new ByteArrayInputStream(secKey), "test", false, out);
		}
		out.println("Signed file to " + signedFile.getAbsolutePath());

		// Verify signed file
		File verifiedFile = new File(System.getProperty("java.io.tmpdir"), "PGPHelperSignatureTest.verified");
		try (FileOutputStream out = new FileOutputStream(verifiedFile)) {
			assertTrue(PGPHelper.verify(signedFile, new ByteArrayInputStream(pubKey), out));
		}
		out.println("Verified file to " + verifiedFile.getAbsolutePath());

		String contents = readContents(verifiedFile);
		assertThat(contents, equalTo(msg));

		assertTrue(file.delete());
		assertTrue(signedFile.delete());
		assertTrue(verifiedFile.delete());
	}

	@Test
	public void testCrypto() throws IOException, CryptoException {

		String msg = "This is a crypto test!";

		// Generate keys
		ByteArrayOutputStream pubout = new ByteArrayOutputStream();
		ByteArrayOutputStream secout = new ByteArrayOutputStream();
		PGPHelper.generate("test", "test", true, pubout, secout);
		byte[] pubKey = pubout.toByteArray();
		byte[] secKey = secout.toByteArray();

		// Generate test file
		File file = generateTestFile("PGPHelperCryptoTest.txt", msg);

		// Encrypt file
		File encryptedFile = new File(System.getProperty("java.io.tmpdir"), "PGPHelperCryptoTest.encrypted");
		try (FileOutputStream out = new FileOutputStream(encryptedFile)) {
			PGPHelper.encrypt(file, new ByteArrayInputStream(pubKey), true, true, out);
		}
		out.println("Encrypted file to " + encryptedFile.getAbsolutePath());

		// Decrypt file
		File decryptedFile = new File(System.getProperty("java.io.tmpdir"), "PGPHelperCryptoTest.decrypted");
		try (FileOutputStream out = new FileOutputStream(decryptedFile)) {
			PGPHelper.decrypt(encryptedFile, new ByteArrayInputStream(pubKey), new ByteArrayInputStream(secKey), "test", out);
		}
		out.println("Decrypted file to " + decryptedFile.getAbsolutePath());

		String contents = readContents(decryptedFile);
		assertThat(contents, equalTo(msg));

		assertTrue(file.delete());
		assertTrue(encryptedFile.delete());
		assertTrue(decryptedFile.delete());
	}
}
