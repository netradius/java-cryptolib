/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.legacy;

import com.netradius.cryptolib.CryptoException;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;

import static java.lang.System.out;
import static org.junit.Assert.assertEquals;

public class CryptoProviderTest {

  private SecureRandom random = new SecureRandom();

  @Test
  public void test() throws CryptoException {
    for (CryptoOption option : CryptoOption.OPTIONS) {
      out.println("Running test with " + option);
      CryptoProvider provider = new CryptoProvider(option);
      if (option.isPasswordBased()) {
        provider.setPassword("thisisatest");
      } else {
        provider.generateKey();
      }
      String str = "Your focus determines your reality.";
      byte[] strBytes = str.getBytes(StandardCharsets.UTF_8);
      EncryptedData encryptedData = provider.encrypt(strBytes);
      byte[] dStrBytes = provider.decrypt(encryptedData);
      String dstr = new String(dStrBytes, StandardCharsets.UTF_8);
      assertEquals(str, dstr);
    }
  }

  @Test
  public void testPBKDF2() throws CryptoException {
    byte[] salt = new byte[128];
    random.nextBytes(salt);
    for (CryptoOption option : CryptoOption.OPTIONS) {
      if (option.isSymmetric() && !option.getAlgorithm().equals("DESede")) {
        out.println("Running PBKDF2 test with " + option.toString());
        CryptoProvider provider = new CryptoProvider(option)
            .setKey("thisatest", salt);
        String str = "Your focus determines your reality.";
        byte[] strBytes = str.getBytes(StandardCharsets.UTF_8);
        EncryptedData encryptedData = provider.encrypt(strBytes);
        byte[] dStrBytes = provider.decrypt(encryptedData);
        String dstr = new String(dStrBytes, StandardCharsets.UTF_8);
        assertEquals(str, dstr);
      }
    }
  }

}
