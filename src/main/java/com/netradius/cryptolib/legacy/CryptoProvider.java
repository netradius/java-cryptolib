/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.legacy;

import com.netradius.cryptolib.BCHelper;
import com.netradius.cryptolib.CryptoException;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

/**
 * This class is intended to ease the development burden working with JCE. This class can be used
 * to generate keys as well encrypt and decrypt data. All JCE related exceptions are wrapped with
 * CryptoException for easy handling and once initialized with a key or key pair, the encrypt and
 * decrypt methods are thread safe.
 *
 * @author Erik R. Jensen
 */
public class CryptoProvider {

  /**
   * The default iteration count used for password based encryption if not specified.
   */
  public static final int DEFAULT_ITERATION_COUNT = 10000;

  protected SecureRandom random = new SecureRandom();
  protected CryptoOption option;
  protected KeyPair keyPair;
  protected Key key;
  protected int iterationCount = DEFAULT_ITERATION_COUNT;

  protected void checkForKey() throws CryptoException {
    if (key == null && keyPair == null) {
      throw new CryptoException("Missing key or key pair");
    }
  }

  /**
   * Creates a new CryptoProvider.
   *
   * @param option the cryptographic option to use
   */
  public CryptoProvider(CryptoOption option) throws CryptoException {
    this.option = option;
    if (option.getProvider() != null && option.getProvider().equals("BC")) {
      BCHelper.register();
    }
  }

  /**
   * Returns the cryptographic option used by this instance.
   *
   * @return the cryptographic option
   */
  public CryptoOption getCryptoOption() {
    return option;
  }

  /**
   * Returns the key pair if one is set.
   *
   * @return the key pair or null if not set or a symmetric cipher is set
   */
  public KeyPair getKeyPair() {
    return keyPair;
  }

  /**
   * Sets the key pair to use.
   *
   * @param keyPair the key pair to use
   * @return this instance
   * @throws CryptoException if the cipher set is not asymmetric
   */
  public CryptoProvider setKeyPair(KeyPair keyPair) throws CryptoException {
    if (option.isAsymmetric()) {
      this.keyPair = keyPair;
      return this;
    }
    throw new CryptoException("Asymmetric keys may not be used with a symmetric cipher");
  }

  /**
   * Generates a new key or key pair for use by the provider. The generated key or key pair
   * may be retrieved using the getKey and getKeyPair methods.
   *
   * @return this instance
   */
  public CryptoProvider generateKey() throws CryptoException {
    if (option.isAsymmetric()) {
      try {
        KeyPairGenerator keyPairGenerator = option.getProvider() != null
            ? KeyPairGenerator.getInstance(option.getAlgorithm(), option.getProvider())
            : KeyPairGenerator.getInstance(option.getAlgorithm());
        keyPairGenerator.initialize(option.getKeySize(), random);
        this.keyPair = keyPairGenerator.generateKeyPair();
      } catch (NoSuchAlgorithmException | NoSuchProviderException x) {
        throw new CryptoException(x);
      }
    } else if (option.isSymmetric()) {
      try {
        KeyGenerator keyGenerator = option.getProvider() != null
            ? KeyGenerator.getInstance(option.getAlgorithm(), option.getProvider())
            : KeyGenerator.getInstance(option.getAlgorithm());
        keyGenerator.init(option.getKeySize(), random);
        this.key = keyGenerator.generateKey();
      } catch (NoSuchAlgorithmException | NoSuchProviderException x) {
        throw new CryptoException(x);
      }
    } else {
      throw new CryptoException("Unable to generate keys for " + option.getTransform());
    }
    return this;
  }

  /**
   * Returns the key if one is set.
   *
   * @return the key or null if not set or an asymmetric cipher is set
   */
  public Key getKey() {
    return key;
  }

  /**
   * Sets the key to use.
   *
   * @param key the key to use
   * @return this instance
   * @throws CryptoException if the cipher set is not symmetric
   */
  public CryptoProvider setKey(Key key) throws CryptoException {
    if (!option.isAsymmetric()) {
      this.key = key;
      return this;
    }
    throw new CryptoException("Symmetric keys may not be used with an asymmetric cipher");
  }

  /**
   * Sets a key using PBKDF2WithHmacSHA1. This mechanism fundamentally different than password
   * based encryption. The password and salt provided are used to generate a key for the transform,
   * such as AES or Twofish. Be aware that this does not work with DESede (3DES).
   *
   * @param password the password to use
   * @param salt the salt to use
   * @param iterationCount the iteration count to use
   * @return this instance
   * @throws CryptoException if PBKDF2 is not supported by the CryptoOption
   */
  public CryptoProvider setKey(String password, byte[] salt, int iterationCount) throws CryptoException {
    if (!option.isSymmetric()) {
      throw new CryptoException("PBKDF2 may only be used with symmetric ciphers");
    }
    if (option.getAlgorithm().equalsIgnoreCase("DESede")) {
      throw new CryptoException("PBKDF2 does not work with DESede");
    }
    try {
      SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
      KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterationCount,
          option.getKeySize());
      SecretKey tmp = factory.generateSecret(spec);
      this.key = new SecretKeySpec(tmp.getEncoded(), option.getAlgorithm());
    } catch (NoSuchAlgorithmException | InvalidKeySpecException x) {
      throw new CryptoException(x);
    }
    return this;
  }

  /**
   * Sets a key using PBKDF2WithHmacSHA1. The default iteration count of 10000 is used. This
   * mechanism fundamentally different than password based encryption. The password and salt
   * provided are used to generate a key for the transform, such as AES or Twofish. Be aware
   * that this does not work with DESede (3DES).
   *
   * @param password the password to use
   * @param salt the salt to use
   * @return this instance
   * @throws CryptoException if PBKDF2 is not supported by the CryptoOption
   */
  public CryptoProvider setKey(String password, byte[] salt) throws CryptoException {
    return setKey(password, salt, DEFAULT_ITERATION_COUNT);
  }

  /**
   * Sets the password to be used to encrypt and decrypt data when using password based encryption.
   *
   * @param password the password to use
   * @return this instance
   * @throws CryptoException if the the cipher is not password based
   */
  public CryptoProvider setPassword(String password) throws CryptoException {
    if (!option.isPasswordBased()) {
      throw new CryptoException("Passwords may only be used with password based encryption");
    }
    try {
      SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(option.getTransform());
      PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray());
      this.key = keyFactory.generateSecret(keySpec);
    } catch (NoSuchAlgorithmException | InvalidKeySpecException x) {
      throw new CryptoException(x);
    }
    return this;
  }

  /**
   * Sets the iteration count to be used when using password based encryption. The default if
   * not specified is 10000.
   *
   * @param iterationCount the iteration count to set
   * @return this instance
   */
  public CryptoProvider setIterationCount(int iterationCount) throws CryptoException {
    if (option.isPasswordBased()) {
      this.iterationCount = iterationCount;
      return this;
    }
    throw new CryptoException("Iteration count may only be used with password based encryption");
  }

  /**
   * Returns a Cipher instance for this provider.
   *
   * @return a Cipher instance
   */
  public Cipher getCipher() throws CryptoException {
    try {
      return option.getProvider() != null
          ? Cipher.getInstance(option.getTransform(), option.getProvider())
          : Cipher.getInstance(option.getTransform());
    } catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException x) {
      throw new CryptoException(x);
    }
  }

  protected byte[] generateIv(Cipher cipher) {
    byte[] buf = new byte[cipher.getBlockSize()];
    random.nextBytes(buf);
    return buf;
  }

  /**
   * Encrypts data.
   *
   * @param data the data to encrypt
   * @return the encrypted data
   */
  public EncryptedData encrypt(byte[] data) throws CryptoException {
    checkForKey();
    Cipher cipher = getCipher();
    byte[] iv = null;
    try {
      if (option.isAsymmetric()) { // Asymmetric
        cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic(), random);
      } else if (option.isSymmetric()) { // Symmetric
        if (option.isIvUsed()) {
          iv = generateIv(cipher);
          cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv), random);
        } else {
          cipher.init(Cipher.ENCRYPT_MODE, key, random);
        }
      } else { // PBE
        iv = generateIv(cipher);
        cipher.init(Cipher.ENCRYPT_MODE, key, new PBEParameterSpec(iv, iterationCount), random);
      }
      return new EncryptedData(cipher.doFinal(data), iv);
    } catch (InvalidKeyException | InvalidAlgorithmParameterException
        | IllegalBlockSizeException | BadPaddingException x) {
      throw new CryptoException(x);
    }
  }

  /**
   * Decrypts data.
   *
   * @param data the data to decrypt
   * @return the decrypted data
   */
  public byte[] decrypt(EncryptedData data) throws CryptoException {
    checkForKey();
    Cipher cipher = getCipher();
    try {
      if (option.isAsymmetric()) { // Asymmetric
        cipher.init(Cipher.DECRYPT_MODE, keyPair.getPrivate(), random);
      } else if (option.isSymmetric()) { // Symmetric
        if (option.isIvUsed()) {
          if (data.getIv() == null || data.getIv().length == 0) {
            throw new CryptoException(
                "No iv is present, but it is required by mode " + option.getMode());
          }
          cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(data.getIv()), random);
        } else {
          cipher.init(Cipher.DECRYPT_MODE, key, random);
        }
      } else { // PBE
        cipher.init(Cipher.DECRYPT_MODE, key, new PBEParameterSpec(data.getIv(), iterationCount),
            random);
      }
      return cipher.doFinal(data.getData());
    } catch (InvalidKeyException | InvalidAlgorithmParameterException
        | IllegalBlockSizeException | BadPaddingException x) {
      throw new CryptoException(x);
    }
  }
}
