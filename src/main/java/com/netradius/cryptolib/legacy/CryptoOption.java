/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.legacy;

/**
 * Holds common cryptographic options to be used with the CryptoProvider. This class was purposely
 * not created as an enum so it can be extended if required.
 *
 * @author Erik R. Jensen
 */
public class CryptoOption {

  public static final CryptoOption AES_ECB_128 = new CryptoOption("AES", "ECB", "PKCS5Padding", 128);
  public static final CryptoOption AES_ECB_192 = new CryptoOption("AES", "ECB", "PKCS5Padding", 192);
  public static final CryptoOption AES_ECB_256 = new CryptoOption("AES", "ECB", "PKCS5Padding", 256);
  public static final CryptoOption AES_CBC_128 = new CryptoOption("AES", "CBC", "PKCS5Padding", 128);
  public static final CryptoOption AES_CBC_192 = new CryptoOption("AES", "CBC", "PKCS5Padding", 192);
  public static final CryptoOption AES_CBC_256 = new CryptoOption("AES", "CBC", "PKCS5Padding", 256);

  public static final CryptoOption BC_AES_ECB_128 = new CryptoOption("AES", "ECB", "PKCS7Padding", 128, "BC");
  public static final CryptoOption BC_AES_ECB_192 = new CryptoOption("AES", "ECB", "PKCS7Padding", 192, "BC");
  public static final CryptoOption BC_AES_ECB_256 = new CryptoOption("AES", "ECB", "PKCS7Padding", 256, "BC");
  public static final CryptoOption BC_AES_CBC_128 = new CryptoOption("AES", "CBC", "PKCS7Padding", 128, "BC");
  public static final CryptoOption BC_AES_CBC_192 = new CryptoOption("AES", "CBC", "PKCS7Padding", 192, "BC");
  public static final CryptoOption BC_AES_CBC_256 = new CryptoOption("AES", "CBC", "PKCS7Padding", 256, "BC");

  public static final CryptoOption DESEDE_CBC_128 = new CryptoOption("DESede", "CBC", "PKCS5Padding", 112);
  public static final CryptoOption DESEDE_CBC_192 = new CryptoOption("DESede", "CBC", "PKCS5Padding", 168);

  public static final CryptoOption BC_DESEDE_CBC_128 = new CryptoOption("DESede", "CBC", "PKCS7Padding", 128, "BC");
  public static final CryptoOption BC_DESEDE_CBC_192 = new CryptoOption("DESede", "CBC", "PKCS7Padding", 192, "BC");

  public static final CryptoOption BC_BLOWFISH_ECB_PKCS7_448 = new CryptoOption("Blowfish", "ECB", "PKCS7Padding", 448, "BC");
  public static final CryptoOption BC_TWOFISH_ECB_PKCS7_128 = new CryptoOption("Twofish", "ECB", "PKCS7Padding", 128, "BC");
  public static final CryptoOption BC_TWOFISH_ECB_PKCS7_192 = new CryptoOption("Twofish", "ECB", "PKCS7Padding", 192, "BC");
  public static final CryptoOption BC_TWOFISH_ECB_PKCS7_256 = new CryptoOption("Twofish", "ECB", "PKCS7Padding", 256, "BC");

  public static final CryptoOption BC_AES_GCM_PKCS7_128 = new CryptoOption("AES", "GCM", "NoPadding", 128, "BC");
  public static final CryptoOption BC_AES_GCM_PKCS7_192 = new CryptoOption("AES", "GCM", "NoPadding", 192, "BC");
  public static final CryptoOption BC_AES_GCM_PKCS7_256 = new CryptoOption("AES", "GCM", "NoPadding", 256, "BC");

  public static final CryptoOption RSA_ECB_PKCS1 = new CryptoOption("RSA", "ECB", "PKCS1Padding", 1024);
  public static final CryptoOption BC_RSA_ECB_PKCS1 = new CryptoOption("RSA", "ECB", "PKCS1Padding", 1024, "BC");

  public static final CryptoOption PBEWithMD5AndDES = new CryptoOption("PBEWithMD5AndDES");
  public static final CryptoOption PBEWithMD5AndTripleDES = new CryptoOption("PBEWithMD5AndTripleDES");
  public static final CryptoOption PBEWithSHA1AndRC2_40 = new CryptoOption("PBEWithSHA1AndRC2_40");
  public static final CryptoOption PBEWithSHA1AndDESede = new CryptoOption("PBEWithSHA1AndDESede");

  public static final CryptoOption PBEWithMD5And128BITAES_CBC_OPENSSL_BC = new CryptoOption("PBEWithMD5And128BITAES-CBC-OPENSSL", "BC");
  public static final CryptoOption PBEWithMD5And192BITAES_CBC_OPENSSL_BC = new CryptoOption("PBEWithMD5And192BITAES-CBC-OPENSSL", "BC");
  public static final CryptoOption PBEWithMD5And256BITAES_CBC_OPENSSL_BC = new CryptoOption("PBEWithMD5And256BITAES-CBC-OPENSSL", "BC");
  public static final CryptoOption PBEWithSHAAnd128BITAES_CBC_BC = new CryptoOption("PBEWithSHAAnd128BITAES-CBC-BC", "BC");
  public static final CryptoOption PBEWithSHAAnd192BITAES_CBC_BC = new CryptoOption("PBEWithSHAAnd192BITAES-CBC-BC", "BC");
  public static final CryptoOption PBEWithSHAAnd256BITAES_CBC_BC = new CryptoOption("PBEWithSHAAnd256BITAES-CBC-BC", "BC");
  public static final CryptoOption PBEWithSHA256And128BITAES_CBC_BC = new CryptoOption("PBEWithSHA256And128BITAES-CBC-BC", "BC");
  public static final CryptoOption PBEWithSHA256And192BITAES_CBC_BC = new CryptoOption("PBEWithSHA256And192BITAES-CBC-BC", "BC");
  public static final CryptoOption PBEWithSHA256And256BITAES_CBC_BC = new CryptoOption("PBEWithSHA256And256BITAES-CBC-BC", "BC");
  public static final CryptoOption PBEWithSHAAnd128BITRC4_BC = new CryptoOption("PBEWithSHAAnd128BITRC4", "BC");
  public static final CryptoOption PBEWithSHAAnd40BITRC4_BC = new CryptoOption("PBEWithSHAAnd40BITRC4", "BC");
  public static final CryptoOption PBEWithMD2AndDES_BC = new CryptoOption("PBEWithMD2AndDES", "BC");
  public static final CryptoOption PBEWithSHA1AndDES_BC = new CryptoOption("PBEWithSHA1AndDES", "BC");
  public static final CryptoOption PBEWithSHAAnd3_KEYTRIPLEDES_CBC_BC = new CryptoOption("PBEWithSHAAnd3-KEYTRIPLEDES-CBC", "BC");
  public static final CryptoOption PBEWithSHAAnd2_KEYTRIPLEDES_CBC_BC = new CryptoOption("PBEWithSHAAnd2-KEYTRIPLEDES-CBC", "BC");
  public static final CryptoOption PBEWithSHAAndIDEA_CBC_BC = new CryptoOption("PBEWithSHAAndIDEA-CBC", "BC");
  public static final CryptoOption PBEWithMD5AndRC2_BC = new CryptoOption("PBEWithMD5AndRC2", "BC");
  public static final CryptoOption PBEWithSHA1AndRC2_BC = new CryptoOption("PBEWithSHA1AndRC2", "BC");
  public static final CryptoOption PBEWithSHAAnd128BITRC2_CBC_BC = new CryptoOption("PBEWithSHAAnd128BITRC2-CBC", "BC");
  public static final CryptoOption PBEWithSHAAnd40BITRC2_CBC_BC = new CryptoOption("PBEWithSHAAnd40BITRC2-CBC", "BC");
  public static final CryptoOption PBEWithSHAAndTWOFISH_CBC_BC = new CryptoOption("PBEWithSHAAndTWOFISH-CBC", "BC");

  public static final CryptoOption[] OPTIONS = {
      AES_ECB_128, AES_ECB_192, AES_ECB_256,
      AES_CBC_128, AES_CBC_192, AES_CBC_256,
      BC_AES_ECB_128, BC_AES_ECB_192, BC_AES_ECB_256,
      BC_AES_CBC_128, BC_AES_CBC_192, BC_AES_CBC_256,
      DESEDE_CBC_128, DESEDE_CBC_192,
      BC_DESEDE_CBC_128, BC_DESEDE_CBC_192,
      BC_BLOWFISH_ECB_PKCS7_448,
      BC_TWOFISH_ECB_PKCS7_128, BC_TWOFISH_ECB_PKCS7_192, BC_TWOFISH_ECB_PKCS7_256,
      BC_AES_GCM_PKCS7_128, BC_AES_GCM_PKCS7_192, BC_AES_GCM_PKCS7_256,
      RSA_ECB_PKCS1, BC_RSA_ECB_PKCS1,
      PBEWithMD5AndDES, PBEWithMD5AndTripleDES, PBEWithSHA1AndRC2_40, PBEWithSHA1AndDESede,
      PBEWithMD5And128BITAES_CBC_OPENSSL_BC, PBEWithMD5And192BITAES_CBC_OPENSSL_BC,
      PBEWithMD5And256BITAES_CBC_OPENSSL_BC,
      PBEWithSHAAnd128BITAES_CBC_BC, PBEWithSHAAnd192BITAES_CBC_BC, PBEWithSHAAnd256BITAES_CBC_BC,
      PBEWithSHA256And128BITAES_CBC_BC, PBEWithSHA256And192BITAES_CBC_BC,
      PBEWithSHA256And256BITAES_CBC_BC,
      PBEWithSHAAnd128BITRC4_BC, PBEWithSHAAnd40BITRC4_BC,
      PBEWithMD2AndDES_BC, PBEWithSHA1AndDES_BC,
      PBEWithSHAAnd3_KEYTRIPLEDES_CBC_BC, PBEWithSHAAnd2_KEYTRIPLEDES_CBC_BC,
      PBEWithSHAAndIDEA_CBC_BC,
      PBEWithMD5AndRC2_BC, PBEWithSHA1AndRC2_BC,
      PBEWithSHAAnd128BITRC2_CBC_BC, PBEWithSHAAnd40BITRC2_CBC_BC, PBEWithSHAAndTWOFISH_CBC_BC
  };

  protected String transform;
  protected String algorithm;
  protected String mode;
  protected String padding;
  protected int keySize = -1;
  protected String provider;

  /**
   * Creates a new CryptoOption for symmetric and asymmetric block ciphers for a specific provider.
   *
   * @param algorithm the algorithm to be used
   * @param mode the mode to be used
   * @param padding the padding to be used
   * @param keySize the key size
   * @param provider the provider
   */
  public CryptoOption(String algorithm, String mode, String padding, int keySize, String provider) {
    this.algorithm = algorithm;
    this.mode = mode;
    this.padding = padding;
    this.keySize = keySize;
    this.provider = provider;
    this.transform = algorithm + "/" + mode + "/" + padding;
  }

  /**
   * Creates a new CryptoOption for symmetric and asymmetric block ciphers using the default
   * provider.
   *
   * @param algorithm the algorithm to be used
   * @param mode the mode to be used
   * @param padding the padding to be used
   * @param keySize the key size
   */
  public CryptoOption(String algorithm, String mode, String padding, int keySize) {
    this(algorithm, mode, padding, keySize, null);
  }

  /**
   * Creates a new CryptoOption for password based encryption (PBE).
   *
   * @param transform the transform to use
   * @param provider the provider
   */
  public CryptoOption(String transform, String provider) {
    this.transform = transform;
    this.provider = provider;
  }

  /**
   * Creates a new CryptoOption for password based encryption (PBE).
   *
   * @param transform the transform to use
   */
  public CryptoOption(String transform) {
    this(transform, null);
  }

  public String getAlgorithm() {
    return algorithm;
  }

  public String getMode() {
    return mode;
  }

  public String getPadding() {
    return padding;
  }

  public int getKeySize() {
    return keySize;
  }

  public String getProvider() {
    return provider;
  }

  public String getTransform() {
    return transform;
  }

  public boolean isAsymmetric() {
    return !isPasswordBased() && algorithm.equals("RSA");
  }

  public boolean isSymmetric() {
    return !isAsymmetric() && !isPasswordBased();
  }

  public boolean isPasswordBased() {
    return transform.startsWith("PBE") || transform.startsWith("PBK");
  }

  public boolean isIvUsed() {
    return !mode.equals("ECB") && !isAsymmetric();
  }

  public String toString() {
    return getTransform()
        + (keySize > 0 ? "-" + keySize : "")
        + (provider == null ? "" : "-" + provider);
  }
}
