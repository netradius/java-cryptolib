/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.provider;

import com.netradius.commons.bitsnbytes.BitTwiddler;
import com.netradius.commons.lang.ArrayHelper;
import com.netradius.commons.lang.ReflectionHelper;
import com.netradius.cryptolib.CryptoException;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.security.Provider;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;

class AESProviderImpl implements AESProvider {

  private static byte[] iv(int length, SecureRandom secureRandom) {
    secureRandom = secureRandom == null ? SECURE_RANDOM : secureRandom;
    byte[] iv = new byte[length];
    secureRandom.nextBytes(iv);
    return iv;
  }

  private static void init(Cipher cipher, int mode, Key key, AlgorithmParameterSpec parameterSpec,
      SecureRandom secureRandom) throws CryptoException {
    try {
      if (secureRandom == null) {
        cipher.init(mode, key, parameterSpec);
      } else {
        cipher.init(mode, key, parameterSpec, secureRandom);
      }
    } catch (InvalidKeyException | InvalidAlgorithmParameterException x) {
      throw new CryptoException("Failed to initialize cipher: " + x.getMessage(), x);
    }
  }

  private static Key key(int size, SecureRandom secureRandom)
      throws CryptoException {
    try {
      KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
      if (secureRandom != null) {
        keyGenerator.init(size, secureRandom);
      } else {
        keyGenerator.init(size);
      }
      return keyGenerator.generateKey();
    } catch (NoSuchAlgorithmException x) {
      throw new CryptoException("Failed to generate AES key: " + x.getMessage(), x);
    }
  }

  static class CBCImpl implements CBC {

    private static final String TRANSFORM = "AES/CBC/PKCS5Padding";

    private Key key;
    private SecureRandom secureRandom;
    private String provider;

    CBCImpl(Key key, SecureRandom secureRandom, String provider) {
      this.key = key;
      this.secureRandom = secureRandom;
      this.provider = provider;
    }

    @Override
    public Key key() {
      return key;
    }

    @Override
    public byte[] keyBytes() {
      return key.getEncoded();
    }

    @Override
    public String keyBase64() {
      return BitTwiddler.tob64str(key.getEncoded());
    }

    @Override
    public Cipher cipher() throws CryptoException {
      try {
        return provider == null ? Cipher.getInstance(TRANSFORM) : Cipher.getInstance(TRANSFORM, provider);
      } catch (NoSuchAlgorithmException | NoSuchPaddingException | NoSuchProviderException x) {
        throw new CryptoException("Failed to create cipher: " + x.getMessage(), x);
      }
    }

    @Override
    public byte[] encrypt(byte[] plaintext) throws CryptoException {
      Cipher cipher = cipher();
      byte[] iv = iv(cipher.getBlockSize(), secureRandom);
      IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
      init(cipher, Cipher.ENCRYPT_MODE, key, ivParameterSpec, secureRandom);
      try {
        byte[] ciphertext = cipher.update(plaintext);
        return ArrayHelper.concat(iv, ciphertext, cipher.doFinal());
      } catch (IllegalBlockSizeException | BadPaddingException x) {
        throw new CryptoException("Failed to encrypt data: " + x.getMessage(), x);
      }
    }

    @Override
    public byte[] decrypt(byte[] ciphertext) throws CryptoException {
      Cipher cipher = cipher();
      byte[] iv = Arrays.copyOfRange(ciphertext, 0, cipher.getBlockSize());
      IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
      init(cipher, Cipher.DECRYPT_MODE, key, ivParameterSpec, secureRandom);
      try {
        byte[] plaintext = cipher.update(ciphertext, cipher.getBlockSize(),
            ciphertext.length - cipher.getBlockSize());
        return ArrayHelper.concat(plaintext, cipher.doFinal());
      } catch (IllegalBlockSizeException | BadPaddingException x) {
        throw new CryptoException("Failed to decrypt data: " + x.getMessage(), x);
      }
    }
  }

  static class GCMImpl implements GCM {

    private static final String TRANSFORM = "AES/GCM/NoPadding";
    private static final int IV_LENGTH = 12;

    private Key key;
    private SecureRandom secureRandom;
    private String provider;
    private int tagLength;
    private int tagLengthBytes;

    GCMImpl(Key key, SecureRandom secureRandom, String provider, int tagLength) {
      this.key = key;
      this.secureRandom = secureRandom;
      this.provider = provider;
      this.tagLength = tagLength;
      this.tagLengthBytes = tagLength / 8;
    }

    @Override
    public Key key() {
      return key;
    }

    @Override
    public byte[] keyBytes() {
      return key.getEncoded();
    }

    @Override
    public String keyBase64() {
      return BitTwiddler.tob64str(key.getEncoded());
    }

    @Override
    public Cipher cipher() throws CryptoException {
      try {
        return provider == null ? Cipher.getInstance(TRANSFORM) : Cipher.getInstance(TRANSFORM, provider);
      } catch (NoSuchAlgorithmException | NoSuchPaddingException | NoSuchProviderException x) {
        throw new CryptoException("Failed to create cipher: " + x.getMessage(), x);
      }
    }

    @Override
    public int ciphertextLength(int plaintextLength) {
      return plaintextLength + IV_LENGTH + tagLengthBytes;
    }

    @Override
    public int plaintextLength(int ciphertextLength) {
      return ciphertextLength - IV_LENGTH - tagLengthBytes;
    }

    @Override
    public int minOutputBufferSize(int inputBufferSize) {
      return IV_LENGTH + inputBufferSize + 16; // The 16 is needed because of a JDK bug
    }

    @Override
    public byte[] encrypt(byte[] plaintext) throws CryptoException {
      return encrypt(plaintext, null);
    }

    @Override
    public byte[] decrypt(byte[] ciphertext) throws CryptoException {
      return decrypt(ciphertext, null);
    }

    @Override
    public byte[] encrypt(byte[] plaintext, byte[] aad) throws CryptoException {
      byte[] ciphertext = new byte[ciphertextLength(plaintext.length)];
      ByteBuffer input = ByteBuffer.wrap(plaintext);
      ByteBuffer output = ByteBuffer.wrap(ciphertext);
      encrypter(aad).update(input, output).complete(output);
      return ciphertext;
    }

    @Override
    public byte[] decrypt(byte[] ciphertext, byte[] aad) throws CryptoException {
      byte[] plaintext = new byte[plaintextLength(ciphertext.length)];
      ByteBuffer input = ByteBuffer.wrap(ciphertext);
      ByteBuffer output = ByteBuffer.wrap(plaintext);
      decrypter(aad).update(input, output).complete(output);
      return plaintext;
    }

    @Override
    public int encrypt(ByteChannel in, ByteChannel out) throws CryptoException, IOException {
      return encrypt(in, out, null);
    }

    @Override
    public int decrypt(ByteChannel in, ByteChannel out) throws CryptoException, IOException {
      return decrypt(in, out, null);
    }

    private int crypt(Crypter c, ByteChannel in, ByteChannel out) throws CryptoException, IOException {
      ByteBuffer ib = ByteBuffer.allocate(4096);
      ByteBuffer ob = ByteBuffer.allocate(minOutputBufferSize(4096));
      int count = 0;
      int read = in.read(ib);
      while (read != -1) {
        if (read > 0) {
          ib.flip();
          c.update(ib, ob);
          ob.flip();
          count += ob.remaining();
          out.write(ob);
          ob.clear();
          ib.clear();
        }
        read = in.read(ib);
      }
      c.complete(ob);
      ob.flip();
      count += ob.remaining();
      out.write(ob);
      return count;
    }

    @Override
    public int encrypt(ByteChannel in, ByteChannel out, byte[] aad) throws CryptoException, IOException {
      return crypt(encrypter(aad), in, out);
    }

    @Override
    public int decrypt(ByteChannel in, ByteChannel out, byte[] aad) throws CryptoException, IOException {
      return crypt(decrypter(aad), in, out);
    }

    @Override
    public Crypter encrypter() throws CryptoException {
      return new GCMCrypterImpl(Crypter.OpMode.ENCRYPT, cipher(), key, secureRandom, IV_LENGTH, tagLength, null);
    }

    @Override
    public Crypter encrypter(byte[] aad) throws CryptoException {
      return new GCMCrypterImpl(Crypter.OpMode.ENCRYPT, cipher(), key, secureRandom, IV_LENGTH, tagLength, aad);
    }

    @Override
    public Crypter decrypter() throws CryptoException {
      return new GCMCrypterImpl(Crypter.OpMode.DECRYPT, cipher(), key, secureRandom, IV_LENGTH, tagLength, null);
    }

    @Override
    public Crypter decrypter(byte[] aad) throws CryptoException {
      return new GCMCrypterImpl(Crypter.OpMode.DECRYPT, cipher(), key, secureRandom, IV_LENGTH, tagLength, aad);
    }
  }

  static class GCMCrypterImpl implements Crypter {

    private GCM gcm;
    private OpMode opMode;
    private Cipher cipher;
    private Key key;
    private SecureRandom secureRandom;
    private int ivLength;
    private int tagLength;
    private byte[] aad;
    private boolean initialized = false;

    GCMCrypterImpl(OpMode opMode, Cipher cipher, Key key, SecureRandom secureRandom,
                   int ivLength, int tagLength, byte[] aad) {
      this.opMode = opMode;
      this.cipher = cipher;
      this.key = key;
      this.secureRandom = secureRandom;
      this.ivLength = ivLength;
      this.tagLength = tagLength;
      this.aad = aad;
    }

    private void encryptInit(ByteBuffer output) throws CryptoException {
      if (output.remaining() < ivLength) {
        throw new CryptoException(String.format("Output buffer has %d bytes remaining and %d bytes are required",
            output.remaining(), ivLength));
      }
      byte[] iv = iv(ivLength, secureRandom);
      GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(tagLength, iv);
      AESProviderImpl.init(cipher, Cipher.ENCRYPT_MODE, key, gcmParameterSpec, secureRandom);
      output.put(iv);
      if (aad != null) {
        cipher.updateAAD(aad);
      }
      initialized = true;
    }

    private void decryptInit(ByteBuffer input) throws CryptoException {
      if (input.remaining() < ivLength) {
        throw new CryptoException(String.format("Input buffer has %d bytes remaining and %d bytes are required",
            input.remaining(), ivLength));
      }
      byte[] iv = new byte[ivLength];
      input.get(iv);
      GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(tagLength, iv);
      AESProviderImpl.init(cipher, Cipher.DECRYPT_MODE, key, gcmParameterSpec, secureRandom);
      if (aad != null) {
        cipher.updateAAD(aad);
      }
      initialized = true;
    }

    @Override
    public Crypter update(ByteBuffer input, ByteBuffer output) throws CryptoException {
      if (!initialized) {
        if (opMode == OpMode.ENCRYPT) {
          encryptInit(output);
        } else {
          decryptInit(input);
        }
      }
      try {
        cipher.update(input, output);
      } catch (ShortBufferException x) {
        throw new CryptoException(x.getMessage(), x);
      }
      return this;
    }

    @Override
    public Crypter complete(ByteBuffer output) throws CryptoException {
      if (!initialized) {
        throw new CryptoException("not initialized");
      }
      try {
        cipher.doFinal(ByteBuffer.wrap(new byte[0]), output);
      } catch (ShortBufferException | IllegalBlockSizeException | BadPaddingException x) {
        throw new CryptoException(x.getMessage(), x);
      }
      return this;
    }
  }

  static class AESBuilderImpl implements AESBuilder {

    int size;
    private Key key;
    private SecureRandom secureRandom;
    private String provider;

    AESBuilderImpl(int size) {
      this.size = size;
    }

    AESBuilderImpl(Key key) {
      this.key = key;
    }

    @Override
    public AESBuilder random(SecureRandom secureRandom) {
      this.secureRandom = secureRandom;
      return this;
    }

    @Override
    public AESBuilder provider(String provider) {
      this.provider = provider;
      return this;
    }

    @Override
    public AESBuilder bcProvider() {
      if (Security.getProvider("BC") == null) {
        @SuppressWarnings("unchecked")
        Class<Provider> clazz = (Class<Provider>)ReflectionHelper
            .findClass("org.bouncycastle.jce.provider.BouncyCastleProvider");
        if (clazz == null) {
          throw new IllegalStateException("Unable to find class org.bouncycastle.jce.provider.BouncyCastleProvider");
        }
        Security.addProvider(ReflectionHelper.newInstance(clazz));
      }
      this.provider = "BC";
      return this;
    }

    @Override
    public CBC cbc() throws CryptoException {
      return new CBCImpl(key == null ? key(size, secureRandom) : key, secureRandom, provider);
    }

    @Override
    public GCM gcm128() throws CryptoException {
      return new GCMImpl(key == null ? key(size, secureRandom) : key, secureRandom, provider, 128);
    }

    @Override
    public GCM gcm120() throws CryptoException {
      return new GCMImpl(key == null ? key(size, secureRandom) : key, secureRandom, provider, 120);
    }

    @Override
    public GCM gcm112() throws CryptoException {
      return new GCMImpl(key == null ? key(size, secureRandom) : key, secureRandom, provider, 112);
    }

    @Override
    public GCM gcm104() throws CryptoException {
      return new GCMImpl(key == null ? key(size, secureRandom) : key, secureRandom, provider, 104);
    }

    @Override
    public GCM gcm96() throws CryptoException {
      return new GCMImpl(key == null ? key(size, secureRandom) : key, secureRandom, provider, 96);
    }
  }

}
