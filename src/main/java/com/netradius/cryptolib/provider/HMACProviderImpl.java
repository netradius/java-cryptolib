/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.provider;

import com.netradius.commons.bitsnbytes.BitTwiddler;
import com.netradius.cryptolib.CryptoException;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

public class HMACProviderImpl implements HMACProvider {

  static class HMACBuilderImpl implements HMACBuilder {

    private Key key;
    private String algorithm;
    private int hashLength;
    private String provider;

    public HMACBuilderImpl(String algorithm, int hashLength) {
      this.algorithm = algorithm;
      this.hashLength = hashLength;
    }

    @Override
    public HMACBuilder key(Key key) {
      this.key = key;
      return this;
    }

    @Override
    public HMACBuilder key(byte[] key) {
      this.key = new SecretKeySpec(key, algorithm);
      return this;
    }

    @Override
    public HMACBuilder key(String b64Key) {
      this.key = new SecretKeySpec(BitTwiddler.fromb64str(b64Key), algorithm);
      return this;
    }

    @Override
    public HMACBuilder provider(String provider) {
      this.provider = provider;
      return this;
    }

    @Override
    public HMAC hmac() throws CryptoException {
      return new HMACImpl(key, algorithm, hashLength, provider);
    }
  }

  static class HMACImpl implements HMAC {

    private Key key;
    private int hashLength;

    HMACImpl(Key key , String algorithm, int hashLength, String provider) throws CryptoException {
      this.key = key;
      if (key == null) {
        try {
          KeyGenerator keyGenerator = provider == null
              ? KeyGenerator.getInstance(algorithm)
              : KeyGenerator.getInstance(algorithm, provider);
          this.key = keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException | NoSuchProviderException x) {
          throw new CryptoException(x.getMessage(), x);
        }
      }
      this.hashLength = hashLength;
    }

    @Override
    public Key key() {
      return key;
    }

    @Override
    public byte[] keyBytes() {
      return key.getEncoded();
    }

    @Override
    public String keyBase64() {
      return BitTwiddler.tob64str(key.getEncoded());
    }

    @Override
    public String algorithm() {
      return key.getAlgorithm();
    }

    @Override
    public int hashLength() {
      return hashLength;
    }

    @Override
    public int hashLengthBytes() {
      return hashLength / 8;
    }

    @Override
    public byte[] encode(byte[] message) throws CryptoException {
      byte[] hash = new byte[hashLength / 8];
      ByteBuffer output = ByteBuffer.wrap(hash);
      encoder().update(ByteBuffer.wrap(message)).complete(output);
      return hash;
    }

    @Override
    public Encoder encoder() throws CryptoException {
      return new EncoderImpl(key);
    }
  }

  static class EncoderImpl implements Encoder {

    private Mac mac;

    EncoderImpl(Key key) throws CryptoException {
      try {
        this.mac = Mac.getInstance(key.getAlgorithm());
        this.mac.init(key);
      } catch (NoSuchAlgorithmException | InvalidKeyException x) {
        throw new CryptoException(x.getMessage(), x);
      }
    }

    @Override
    public Encoder update(ByteBuffer input) {
      mac.update(input);
      return this;
    }

    @Override
    public Encoder complete(ByteBuffer output) {
      byte[] hash = mac.doFinal();
      output.put(hash);
      return this;
    }
  }
}
