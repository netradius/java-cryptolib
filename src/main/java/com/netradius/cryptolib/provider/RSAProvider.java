/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.provider;

import com.netradius.cryptolib.CryptoException;

import javax.crypto.Cipher;
import java.security.KeyPair;
import java.security.SecureRandom;

/**
 * Provides an easy to use and less error prone abstraction on top of Java Cryptography Extensions (JCE).
 * This provide specifically provides support for RSA based encryption.
 */
public interface RSAProvider extends Provider {

  /**
   * The default transform used by the provider if none is provided.
   */
  String DEFAULT_TRANSFORM = "RSA/NONE/OAEPWithSHA3-512AndMGF1Padding";

  /**
   * Get a RSA 2048 bit builder.
   *
   * @return the builder
   */
  static RSABuilder rsa2048() {
    return new RSAProviderImpl.RSABuilderImpl(2048);
  }

  /**
   * Get a RSA 2048 bit builder.
   *
   * @return the builder
   */
  static RSABuilder rsa3072() {
    return new RSAProviderImpl.RSABuilderImpl(3072);
  }

  /**
   * Get a RSA 4096 bit builder.
   *
   * @return the builder
   */
  static RSABuilder rsa4096() {
    return new RSAProviderImpl.RSABuilderImpl(4096);
  }

  /**
   * Get a RSA builder for an existing key pair.
   *
   * @return the builder
   */
  static RSABuilder rsa(KeyPair keyPair) {
    return new RSAProviderImpl.RSABuilderImpl(keyPair);
  }

  /**
   * Builder used to generate RSA providers.
   */
  interface RSABuilder {

    /**
     * Set a secure random number generator to use instead of a default one.
     *
     * @param secureRandom the secure random number generator to use
     * @return this instance
     */
    RSABuilder random(SecureRandom secureRandom);

    /**
     * Sets the transform to be used by the provider. If one is not provided a default is used.
     *
     * @param transform the transform to use
     * @return this instance
     */
    RSABuilder transform(String transform);

    /**
     * Gets an RSA provider.
     *
     * @return the RSA provider
     * @throws CryptoException if an error occurs obtaining the provider
     */
    RSA rsa() throws CryptoException;

  }

  interface RSA {

    /**
     * Get the key pair used by this provider.
     *
     * @return the key pair
     */
    KeyPair keyPair();

    /**
     * Get the cipher used by this provider.
     *
     * @return the cipher
     * @throws CryptoException if an error occurs getting the cipher
     */
    Cipher cipher() throws CryptoException;

    /**
     * Encrypts the provided data.
     *
     * @param plaintext the data to encrypt
     * @return the encrypted data
     * @throws CryptoException if an error occurs encrypting the data
     */
    byte[] encrypt(byte[] plaintext) throws CryptoException;

    /**
     * Decrypts the provided data.
     *
     * @param ciphertext the encrypted data to decrypt
     * @return the decrypted data
     * @throws CryptoException if an error occurs decrypting the data
     */
    byte[] decrypt(byte[] ciphertext) throws CryptoException;

  }
}
