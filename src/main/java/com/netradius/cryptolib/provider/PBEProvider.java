/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.provider;

import com.netradius.cryptolib.CryptoException;

import javax.crypto.Cipher;
import javax.crypto.spec.PBEKeySpec;
import java.security.SecureRandom;

/**
 * Provides an easy to use and less error prone abstraction on top of Java Cryptography Extensions (JCE)
 * This provider specifically provides support password base encryption.
 */
public interface PBEProvider extends Provider {

  /**
   * The default number of iterations used providers if none is provided.
   */
  int DEFAULT_ITERATIONS = 200000;

  /**
   * The default recommended salt size for SHA1.
   */
  int DEFAULT_SHA_SALT_SIZE = 20; // 160 bit hash / 8

  /**
   * The default recommended salt size for SHA224.
   */
  int DEFAULT_SHA224_SALT_SIZE = 28; // 224 bit hash / 8

  /**
   * The default recommended salt size for SHA256.
   */
  int DEFAULT_SHA256_SALT_SIZE = 32; // 256 bit hash / 8

  /**
   * The default recommended salt size for SHA384.
   */
  int DEFAULT_SHA384_SALT_SIZE = 48; // 384 bit hash / 8

  /**
   * The default recommended salt size for SHA512.
   */
  int DEFAULT_SHA512_SALT_SIZE = 64; // 512 bit hash / 8

  /**
   * Get a PBE builder.
   *
   * @param password the password
   * @return the builder
   */
  static PBEHashBuilder pbe(char[] password) {
    return new PBEProviderImpl.PBEHashBuilderImpl(password);
  }

  /**
   * Builder used to generate hash based provider builders.
   */
  interface PBEHashBuilder {

    /**
     * Set a secure random number generator to use instead of a default one.
     *
     * @param secureRandom the secure random number generator to use
     * @return this instance
     */
    PBEHashBuilder random(SecureRandom secureRandom);

    /**
     * Set the salt to be used by this provider when generating the key.
     * @param salt the salt to use
     * @return this instance
     */
    PBEHashBuilder salt(byte[] salt);

    /**
     * Set the number of iterations used by this provider when generating the key.
     *
     * @param iterations the number of iterations to use
     * @return this instance
     */
    PBEHashBuilder iterations(int iterations);

    /**
     * Get a SHA1 builder.
     *
     * @return the builder
     */
    ShaBuilder sha();

    /**
     * Get a SHA256 builder.
     *
     * @return the builder
     */
    ShaBuilder sha256();

    /**
     * Get a HMAC SHA1 builder.
     *
     * @return the builder
     */
    HmacShaBuilder hmacSha1();

    /**
     * Get a HMAC SHA224 builder.
     *
     * @return the builder
     */
    HmacShaBuilder hmacSha224();

    /**
     * Get a HMAC SHA256 builder.
     *
     * @return the builder
     */
    HmacShaBuilder hmacSha256();

    /**
     * Get a HMAC SHA384 builder.
     *
     * @return the builder
     */
    HmacShaBuilder hmacSha384();

    /**
     * Get a HMAC SHA512 builder.
     *
     * @return the builder
     */
    HmacShaBuilder hmacSha512();

  }

  /**
   * Builder used to generate hash based providers.
   */
  interface ShaBuilder {

    /**
     * Get a provider that uses Twofish encryption
     *
     * @return the provider
     * @throws CryptoException if an error occurs obtaining the provider
     */
    PBE twofish() throws CryptoException;

    /**
     * Get a provider that uses AWS 128 bit encryption.
     *
     * @return the provider
     * @throws CryptoException if an error occurs obtaining the provider
     */
    PBE aes128() throws CryptoException;

    /**
     * Get a provider that uses AWS 192 bit encryption.
     *
     * @return the provider
     * @throws CryptoException if an error occurs obtaining the provider
     */
    PBE aes192() throws CryptoException;

    /**
     * Get a provider that uses AWS 256 bit encryption.
     *
     * @return the provider
     * @throws CryptoException if an error occurs obtaining the provider
     */
    PBE aes256() throws CryptoException;

  }

  /**
   * Builder used to generate HMAC hash based providers.
   */
  interface HmacShaBuilder {

    /**
     * Get a provider that uses AWS 128 bit encryption.
     *
     * @return the provider
     * @throws CryptoException if an error occurs obtaining the provider
     */
    PBE aes128() throws CryptoException;

    /**
     * Get a provider that uses AWS 256 bit encryption.
     *
     * @return the provider
     * @throws CryptoException if an error occurs obtaining the provider
     */
    PBE aes256() throws CryptoException;

  }

  interface PBE {

    /**
     * Get the salt used by this provider.
     *
     * @return the salt
     */
    byte[] salt();

    /**
     * Get the number of iterations used by this provider.
     *
     * @return the number of iterations
     */
    int iterations();

    /**
     * Get the key spec used by this provider.
     *
     * @return the key spec
     */
    PBEKeySpec keySpec();

    /**
     * Get the cipher used by this provider.
     *
     * @return the cipher
     * @throws CryptoException if an error occurs getting the cipher
     */
    Cipher cipher() throws CryptoException;

    /**
     * Encrypts the provided data.
     *
     * @param plaintext the data to encrypt
     * @return the encrypted data
     * @throws CryptoException if an error occurs encrypting the data
     */
    byte[] encrypt(byte[] plaintext) throws CryptoException;

    /**
     * Decrypts the provided data.
     *
     * @param ciphertext the encrypted data to decrypt
     * @return the decrypted data
     * @throws CryptoException if an error occurs decrypting the data
     */
    byte[] decrypt(byte[] ciphertext) throws CryptoException;

  }
}
