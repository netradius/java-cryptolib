/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.provider;

import com.netradius.commons.bitsnbytes.BitTwiddler;
import com.netradius.cryptolib.CryptoException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.security.Key;
import java.security.SecureRandom;

/**
 * Provides an easy to use and less error prone abstraction on top of Java Cryptography Extensions (JCE).
 * This provider specifically provides support for AES encryption.
 */
public interface AESProvider extends Provider {

  /**
   * Get an AES 128 bit builder.
   *
   * @return the builder
   */
  static AESBuilder aes128() {
    return new AESProviderImpl.AESBuilderImpl(128);
  }

  /**
   * Get an AES 192 bit builder.
   *
   * @return the builder
   */
  static AESBuilder aes192() {
    return new AESProviderImpl.AESBuilderImpl(192);
  }

  /**
   * Get an AES 256 bit builder.
   *
   * @return the builder
   */
  static AESBuilder aes256() {
    return new AESProviderImpl.AESBuilderImpl(256);
  }

  /**
   * Get an AES builder using an existing AES key.
   *
   * @return the builder
   */
  static AESBuilder aes(Key key) {
    return new AESProviderImpl.AESBuilderImpl(key);
  }

  /**
   * Get an AES builder using an existing AES key.
   *
   * @return the builder
   */
  static AESBuilder aes(byte[] key) {
    SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
    return new AESProviderImpl.AESBuilderImpl(keySpec);
  }

  /**
   * Get an AES builder using an existing AES key.
   *
   * @return the builder
   */
  static AESBuilder aes(String b64) {
    byte[] key = BitTwiddler.fromb64str(b64);
    return aes(key);
  }

  /**
   * Builder used to generate Chain Block Cipher (CBC) and Galois Counter Mode (GCM) providers.
   */
  interface AESBuilder {

    /**
     * Set a secure random number generator to use instead of a default one.
     *
     * @param secureRandom the secure random number generator to use
     * @return this instance
     */
    AESBuilder random(SecureRandom secureRandom);

    /**
     * Set a provider to use instead of the default one.
     *
     * @param provider the provider to use
     * @return this instance
     */
    AESBuilder provider(String provider);

    /**
     * Sets the provider to bouncy castle.
     *
     * @return this instance
     */
    AESBuilder bcProvider();

    /**
     * Get a provider using Chain Block Cipher (CBC).
     *
     * @return the provider
     * @throws CryptoException if an error occurs obtaining the provider
     */
    CBC cbc() throws CryptoException;

    /**
     * Get a provider using Galois Counter Mode (GCM) with a 128 bit authentication tag.
     *
     * @return the provider
     * @throws CryptoException if an error occurs obtaining the provider
     */
    GCM gcm128() throws CryptoException;

    /**
     * Get a provider using Galois Counter Mode (GCM) with a 120 bit authentication tag.
     *
     * @return the provider
     * @throws CryptoException if an error occurs obtaining the provider
     */
    GCM gcm120() throws CryptoException;

    /**
     * Get a provider using Galois Counter Mode (GCM) with a 112 bit authentication tag.
     *
     * @return the provider
     * @throws CryptoException if an error occurs obtaining the provider
     */
    GCM gcm112() throws CryptoException;

    /**
     * Get a provider using Galois Counter Mode (GCM) with a 104 bit authentication tag.
     *
     * @return the provider
     * @throws CryptoException if an error occurs obtaining the provider
     */
    GCM gcm104() throws CryptoException;

    /**
     * Get a provider using Galois Counter Mode (GCM) with a 96 bit authentication tag.
     *
     * @return the provider
     * @throws CryptoException if an error occurs obtaining the provider
     */
    GCM gcm96() throws CryptoException;

  }

  /**
   * A Chain Block Cipher (CBC) provider used to encrypt and decrypt data.
   */
  interface CBC {

    /**
     * Get the key used by this provider.
     *
     * @return the key
     */
    Key key();

    /**
     * Get the key used by this provider.
     *
     * @return the key
     */
    byte[] keyBytes();

    /**
     * Get the key used by this provider.
     *
     * @return the key
     */
    String keyBase64();

    /**
     * Get the cipher used by this provider.
     *
     * @return the cipher
     * @throws CryptoException if an error occurs getting the cipher
     */
    Cipher cipher() throws CryptoException;

    /**
     * Encrypts the provided data.
     *
     * @param plaintext the data to encrypt
     * @return the encrypted data
     * @throws CryptoException if an error occurs encrypting the data
     */
    byte[] encrypt(byte[] plaintext) throws CryptoException;

    /**
     * Decrypts the provided data.
     *
     * @param ciphertext the encrypted data to decrypt
     * @return the decrypted data
     * @throws CryptoException if an error occurs decrypting the data
     */
    byte[] decrypt(byte[] ciphertext) throws CryptoException;

  }

  /**
   * A Galois Counter Mode (GCM) provider used to encrypt and decrypt data.
   */
  interface GCM {

    /**
     * Get the key used by this provider.
     *
     * @return the key
     */
    Key key();

    /**
     * Get the key used by this provider.
     *
     * @return the key
     */
    byte[] keyBytes();

    /**
     * Get the key used by this provider.
     *
     * @return the key
     */
    String keyBase64();

    /**
     * Get the cipher used by this provider.
     *
     * @return the cipher
     * @throws CryptoException if an error occurs getting the cipher
     */
    Cipher cipher() throws CryptoException;

    /**
     * Returns the number of bytes of the ciphertext from the number of bytes of plaintext.
     *
     * @param plaintextLength number of bytes of plaintext data
     * @return the number of bytes of ciphertext data
     */
    int ciphertextLength(int plaintextLength);

    /**
     * Returns the number of bytes of the plaintext from the number of bytes of ciphertext.
     *
     * @param ciphertextLength number of bytes of ciphertext data
     * @return the number of bytes of plaintext data
     */
    int plaintextLength(int ciphertextLength);

    /**
     * Returns the minimum buffer size to allow encryption or decryption operations to succeed.
     *
     * @param inputBufferSize the input buffer size
     * @return the minimum output buffer size required
     */
    int minOutputBufferSize(int inputBufferSize);

    /**
     * Encrypts the provided data.
     *
     * @param plaintext the data to encrypt
     * @return the encrypted data
     * @throws CryptoException if an error occurs encrypting the data
     */
    byte[] encrypt(byte[] plaintext) throws CryptoException;

    /**
     * Decrypts the provided data.
     *
     * @param ciphertext the encrypted data to decrypt
     * @return the decrypted data
     * @throws CryptoException if an error occurs decrypting the data
     */
    byte[] decrypt(byte[] ciphertext) throws CryptoException;

    /**
     * Encrypts the provided data with the provided authentication data.
     *
     * @param plaintext the data to encrypt
     * @param aad the authentication data
     * @return the encrypted data
     * @throws CryptoException if an error occurs encrypting the data
     */
    byte[] encrypt(byte[] plaintext, byte[] aad) throws CryptoException;

    /**
     * Decrypts the provided data with the provided authentication data.
     *
     * @param ciphertext the encrypted data to decrypt
     * @param aad the authentication data
     * @return the decrypted data
     * @throws CryptoException if an error occurs decrypting the data
     */
    byte[] decrypt(byte[] ciphertext, byte[] aad) throws CryptoException;

    /**
     * Encrypts the provided data.
     *
     * @param in the data to encrypt
     * @param out the encrypted data
     * @return the number of bytes written
     * @throws CryptoException if an error occurs
     * @throws IOException if an I/O error occurs
     */
    int encrypt(ByteChannel in, ByteChannel out) throws CryptoException, IOException;

    /**
     * Decrypts the provided data.
     *
     * @param in the data to encrypt
     * @param out the encrypted data
     * @return the number of bytes written
     * @throws CryptoException if an error occurs
     * @throws IOException if an I/O error occurs
     */
    int decrypt(ByteChannel in, ByteChannel out) throws CryptoException, IOException;

    /**
     * Decrypts the provided data with the provided authentication data.
     *
     * @param in the data to encrypt
     * @param out the encrypted data
     * @param aad the authentication data
     * @return the number of bytes written
     * @throws CryptoException if an error occurs
     * @throws IOException if an I/O error occurs
     */
    int encrypt(ByteChannel in, ByteChannel out, byte[] aad) throws CryptoException, IOException;

    /**
     * Encrypts the provided data with the provided authentication data.
     *
     * @param in the data to encrypt
     * @param out the encrypted data
     * @param aad the authentication data
     * @return the number of bytes written
     * @throws CryptoException if an error occurs
     * @throws IOException if an I/O error occurs
     */
    int decrypt(ByteChannel in, ByteChannel out, byte[] aad) throws CryptoException, IOException;

    /**
     * Creates a Crypter used for encryption.
     *
     * @return a Crypter
     * @throws CryptoException if an error occurs
     */
    Crypter encrypter() throws CryptoException;

    /**
     * Creates a Crypter used for encryption.
     *
     * @param aad the authentication data
     * @return a Crypter
     * @throws CryptoException if an error occurs
     */
    Crypter encrypter(byte[] aad) throws CryptoException;

    /**
     * Creates a Crypter used for decryption.
     *
     * @return a Crypter
     * @throws CryptoException if an error occurs
     */
    Crypter decrypter() throws CryptoException;

    /**
     * Creates a Crypter used for decryption.
     *
     * @param aad the authentication data
     * @return a Crypter
     * @throws CryptoException if an error occurs
     */
    Crypter decrypter(byte[] aad) throws CryptoException;
  }

  interface Crypter {

    enum OpMode {
      ENCRYPT,
      DECRYPT
    }

    Crypter update(ByteBuffer input, ByteBuffer output) throws CryptoException;

    Crypter complete(ByteBuffer output) throws CryptoException;

  }

}
