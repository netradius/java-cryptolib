/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.provider;

import com.netradius.commons.lang.ArrayHelper;
import com.netradius.cryptolib.BCHelper;
import com.netradius.cryptolib.CryptoException;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

public class PBEProviderImpl implements PBEProvider {

  private static byte[] generateSalt(int size, SecureRandom secureRandom) {
    byte[] salt = new byte[size];
    if (secureRandom == null) {
      SECURE_RANDOM.nextBytes(salt);
    } else {
      SECURE_RANDOM.nextBytes(salt);
    }
    return salt;
  }

  private static byte[] iv(int length, SecureRandom secureRandom) {
    secureRandom = secureRandom == null ? SECURE_RANDOM : secureRandom;
    byte[] iv = new byte[length];
    secureRandom.nextBytes(iv);
    return iv;
  }

  private static void init(Cipher cipher, int mode, Key key, AlgorithmParameterSpec parameterSpec,
      SecureRandom secureRandom) throws CryptoException {
    try {
      if (secureRandom == null) {
        cipher.init(mode, key, parameterSpec);
      } else {
        cipher.init(mode, key, parameterSpec, secureRandom);
      }
    } catch (InvalidKeyException | InvalidAlgorithmParameterException x) {
      throw new CryptoException("Failed to initialize cipher: " + x.getMessage(), x);
    }
  }

  private static Key key(String transform, PBEKeySpec keySpec)
      throws CryptoException {
    try {
      SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(transform);
      return secretKeyFactory.generateSecret(keySpec);
    } catch (NoSuchAlgorithmException | InvalidKeySpecException x) {
      throw new CryptoException("Failed to generate key: " + x.getMessage(), x);
    }
  }

  static class PBEHashBuilderImpl implements PBEHashBuilder {

    private char[] password;
    private SecureRandom secureRandom;
    private byte[] salt;
    private Integer iterations;

    PBEHashBuilderImpl(char[] password) {
      this.password = password;
    }


    @Override
    public PBEHashBuilder random(SecureRandom secureRandom) {
      this.secureRandom = secureRandom;
      return this;
    }

    @Override
    public PBEHashBuilder salt(byte[] salt) {
      this.salt = salt;
      return this;
    }

    @Override
    public PBEHashBuilder iterations(int iterations) {
      this.iterations = iterations;
      return this;
    }

    @Override
    public ShaBuilder sha() {
      if (salt == null) {
        salt = generateSalt(DEFAULT_SHA_SALT_SIZE, secureRandom);
      }
      if (iterations == null) {
        iterations = DEFAULT_ITERATIONS;
      }
      return new ShaBuilderImpl("PBEWithSHA", password, salt, iterations, secureRandom);
    }

    @Override
    public ShaBuilder sha256() {
      if (salt == null) {
        salt = generateSalt(DEFAULT_SHA256_SALT_SIZE, secureRandom);
      }
      if (iterations == null) {
        iterations = DEFAULT_ITERATIONS;
      }
      return new ShaBuilderImpl("PBEWithSHA256", password, salt, iterations, secureRandom);
    }

    @Override
    public HmacShaBuilder hmacSha1() {
      if (salt == null) {
        salt = generateSalt(DEFAULT_SHA_SALT_SIZE, secureRandom);
      }
      if (iterations == null) {
        iterations = DEFAULT_ITERATIONS;
      }
      return new HmacShaBuilderImpl("PBEWithHmacSHA1", password, salt, iterations, secureRandom);
    }

    @Override
    public HmacShaBuilder hmacSha224() {
      if (salt == null) {
        salt = generateSalt(DEFAULT_SHA224_SALT_SIZE, secureRandom);
      }
      if (iterations == null) {
        iterations = DEFAULT_ITERATIONS;
      }
      return new HmacShaBuilderImpl("PBEWithHmacSHA224", password, salt, iterations, secureRandom);
    }

    @Override
    public HmacShaBuilder hmacSha256() {
      if (salt == null) {
        salt = generateSalt(DEFAULT_SHA256_SALT_SIZE, secureRandom);
      }
      if (iterations == null) {
        iterations = DEFAULT_ITERATIONS;
      }
      return new HmacShaBuilderImpl("PBEWithHmacSHA256", password, salt, iterations, secureRandom);
    }

    @Override
    public HmacShaBuilder hmacSha384() {
      if (salt == null) {
        salt = generateSalt(DEFAULT_SHA384_SALT_SIZE, secureRandom);
      }
      if (iterations == null) {
        iterations = DEFAULT_ITERATIONS;
      }
      return new HmacShaBuilderImpl("PBEWithHmacSHA384", password, salt, iterations, secureRandom);
    }

    @Override
    public HmacShaBuilder hmacSha512() {
      if (salt == null) {
        salt = generateSalt(DEFAULT_SHA512_SALT_SIZE, secureRandom);
      }
      if (iterations == null) {
        iterations = DEFAULT_ITERATIONS;
      }
      return new HmacShaBuilderImpl("PBEWithHmacSHA512", password, salt, iterations, secureRandom);
    }
  }

  static class ShaBuilderImpl implements ShaBuilder {

    private String  hash;
    private char[] password;
    private byte[] salt;
    private int iterations;
    private SecureRandom secureRandom;

    ShaBuilderImpl(String hash, char[] password, byte[] salt, int iterations, SecureRandom secureRandom) {
      this.hash = hash;
      this.password = password;
      this.salt = salt;
      this.iterations = iterations;
      this.secureRandom = secureRandom;
    }

    @Override
    public PBE twofish() throws CryptoException{
      BCHelper.register();
      return new PBEImpl(hash + "AndTwofish-CBC", password, salt, iterations, secureRandom);
    }

    @Override
    public PBE aes128() throws CryptoException {
      return new PBEImpl(hash + "And128BitAES-CBC-BC", password, salt, iterations, secureRandom);
    }

    @Override
    public PBE aes192() throws CryptoException {
      return new PBEImpl(hash + "And192BitAES-CBC-BC", password, salt, iterations, secureRandom);
    }

    @Override
    public PBE aes256() throws CryptoException {
      return new PBEImpl(hash + "And256BitAES-CBC-BC", password, salt, iterations, secureRandom);
    }
  }

  static class HmacShaBuilderImpl implements HmacShaBuilder {

    private String hash;
    private char[] password;
    private byte[] salt;
    private int iterations;
    private SecureRandom secureRandom;

    HmacShaBuilderImpl(String hash, char[] password, byte[] salt, int iterations, SecureRandom secureRandom) {
      this.hash = hash;
      this.password = password;
      this.salt = salt;
      this.iterations = iterations;
      this.secureRandom = secureRandom;
    }

    @Override
    public PBE aes128() throws CryptoException {
      return new HmacPBEImpl(hash + "AndAES_128", password, salt, iterations, secureRandom);
    }

    @Override
    public PBE aes256() throws CryptoException {
      return new HmacPBEImpl(hash + "AndAES_256", password, salt, iterations, secureRandom);
    }
  }

  static abstract class PBEAdapter implements PBE {

    protected String transform;
    protected PBEKeySpec keySpec;
    protected Key key;
    protected SecureRandom secureRandom;

    PBEAdapter(String transform, char[] password, byte[] salt, int iterations, SecureRandom secureRandom)
        throws CryptoException {
      this.transform = transform;
      keySpec = new PBEKeySpec(password, salt, iterations);
      key = key(transform, keySpec);
      this.secureRandom =secureRandom;
    }

    @Override
    public byte[] salt() {
      return keySpec.getSalt();
    }

    @Override
    public int iterations() {
      return keySpec.getIterationCount();
    }

    @Override
    public PBEKeySpec keySpec() {
      return keySpec;
    }

    @Override
    public Cipher cipher() throws CryptoException {
      try {
        return Cipher.getInstance(transform);
      } catch (NoSuchAlgorithmException | NoSuchPaddingException x) {
        throw new CryptoException("Failed to create cipher: " + x.getMessage(), x);
      }
    }

  }

  static class PBEImpl extends PBEAdapter implements PBE {

    PBEImpl(String transform, char[] password, byte[] salt, int iterations, SecureRandom secureRandom)
        throws CryptoException {
      super(transform, password, salt, iterations, secureRandom);
    }

    @Override
    public byte[] encrypt(byte[] plaintext) throws CryptoException {
      Cipher cipher = cipher();
      byte[] iv = iv(cipher.getBlockSize(), secureRandom);
      IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
      init(cipher, Cipher.ENCRYPT_MODE, key, ivParameterSpec, secureRandom);
      try {
        byte[] ciphertext = cipher.update(plaintext);
        return ArrayHelper.concat(iv, ciphertext, cipher.doFinal());
      } catch (IllegalBlockSizeException | BadPaddingException x) {
        throw new CryptoException("Failed to encrypt data: " + x.getMessage(), x);
      }
    }

    @Override
    public byte[] decrypt(byte[] ciphertext) throws CryptoException {
      Cipher cipher = cipher();
      byte[] iv = Arrays.copyOfRange(ciphertext, 0, cipher.getBlockSize());
      IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
      init(cipher, Cipher.DECRYPT_MODE, key, ivParameterSpec, secureRandom);
      try {
        byte[] plaintext = cipher.update(ciphertext, cipher.getBlockSize(),
            ciphertext.length - cipher.getBlockSize());
        return ArrayHelper.concat(plaintext, cipher.doFinal());
      } catch (IllegalBlockSizeException | BadPaddingException x) {
        throw new CryptoException("Failed to decrypt data: " + x.getMessage(), x);
      }
    }
  }

  static class HmacPBEImpl extends PBEAdapter implements PBE {

    HmacPBEImpl(String transform, char[] password, byte[] salt, int iterations, SecureRandom secureRandom) throws CryptoException {
      super(transform, password, salt, iterations, secureRandom);
    }

    @Override
    public byte[] encrypt(byte[] plaintext) throws CryptoException {
      Cipher cipher = cipher();
      byte[] iv = iv(cipher.getBlockSize(), secureRandom);
      IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
      PBEParameterSpec pbeParameterSpec = new PBEParameterSpec(salt(), iterations(), ivParameterSpec);
      init(cipher, Cipher.ENCRYPT_MODE, key, pbeParameterSpec, secureRandom);
      try {
        byte[] ciphertext = cipher.update(plaintext);
        return ArrayHelper.concat(iv, ciphertext, cipher.doFinal());
      } catch (IllegalBlockSizeException | BadPaddingException x) {
        throw new CryptoException("Failed to encrypt data: " + x.getMessage(), x);
      }
    }

    @Override
    public byte[] decrypt(byte[] ciphertext) throws CryptoException {
      Cipher cipher = cipher();
      byte[] iv = Arrays.copyOfRange(ciphertext, 0, cipher.getBlockSize());
      IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
      PBEParameterSpec pbeParameterSpec = new PBEParameterSpec(salt(), iterations(), ivParameterSpec);
      init(cipher, Cipher.DECRYPT_MODE, key, pbeParameterSpec, secureRandom);
      try {
        byte[] plaintext = cipher.update(ciphertext, cipher.getBlockSize(),
            ciphertext.length - cipher.getBlockSize());
        return ArrayHelper.concat(plaintext, cipher.doFinal());
      } catch (IllegalBlockSizeException | BadPaddingException x) {
        throw new CryptoException("Failed to decrypt data: " + x.getMessage(), x);
      }
    }
  }
}
