/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.provider;

import com.netradius.cryptolib.CryptoException;

import java.nio.ByteBuffer;
import java.security.Key;

/**
 * Provides an easy to use and less error prone HMAC abstraction on top of Java Cryptography Extensions (JCE).
 */
public interface HMACProvider extends Provider {

  /**
   * Gets a new HMAC SHA1 builder.
   *
   * @return the builder
   */
  static HMACBuilder sha1() {
    return new HMACProviderImpl.HMACBuilderImpl("HmacSHA1", 160);
  }

  /**
   * Gets a new HMAC SHA256 builder.
   *
   * @return the builder
   */
  static HMACBuilder sha256() {
    return new HMACProviderImpl.HMACBuilderImpl("HmacSHA256", 256);
  }

  /**
   * Gets a new HMAC SHA512 builder.
   *
   * @return the builder
   */
  static HMACBuilder sha512() {
    return new HMACProviderImpl.HMACBuilderImpl("HmacSHA512", 512);
  }

  /**
   * Builder used to generate HMAC providers.
   */
  interface HMACBuilder {

    /**
     * Sets a key to use. If a key is not set, one is generated.
     *
     * @param key the key to use
     * @return this builder
     */
    HMACBuilder key(Key key);

    /**
     * Sets a key to use. If a key is not set, one is generated.
     *
     * @param key the key to use
     * @return this builder
     */
    HMACBuilder key(byte[] key);

    /**
     * Sets a key to use. If a key is not set, one is generated.
     *
     * @param b64Key the key to use
     * @return this builder
     */
    HMACBuilder key(String b64Key);

    /**
     * Sets a provider to use. This is optional.
     *
     * @param provider the specific provider to use.
     * @return this builder
     */
    HMACBuilder provider(String provider);

    /**
     * Gets a new HMAC instance.
     *
     * @return an HMAC instance
     * @throws CryptoException if an error occurs
     */
    HMAC hmac() throws CryptoException;

  }

  /**
   * An HMAC used to generate message authentication codes.
   */
  interface HMAC {

    /**
     * Gets the key used by this HMAC.
     *
     * @return the key
     */
    Key key();

    /**
     * Gets the key used by this HMAC.
     *
     * @return the key
     */
    byte[] keyBytes();

    /**
     * Gets the key used by this HMAC.
     *
     * @return the key
     */
    String keyBase64();

    /**
     * Gets the algorithm used by this HMAC.
     *
     * @return the algorithm
     */
    String algorithm();

    /**
     * Gets the hash length used by this HMAC in bits.
     *
     * @return the hash length in bits
     */
    int hashLength();

    /**
     * Gets the hash length used by this HMAC ib bytes.
     *
     * @return the hash length in bytes
     */
    int hashLengthBytes();

    /**
     * Encodes a message.
     *
     * @param message the message to be encoded
     * @return the hash
     * @throws CryptoException if an error occurs
     */
    byte[] encode(byte[] message) throws CryptoException;

    /**
     * Returns an encoder used to encode long messages.
     *
     * @return an encoder
     * @throws CryptoException if an error occurs
     */
    Encoder encoder() throws CryptoException;

  }

  /**
   * An HMAC encoder.
   */
  interface Encoder {

    /**
     * Adds message data for encoding.
     *
     * @param input the message data to encode
     * @return this encoder
     */
    Encoder update(ByteBuffer input);

    /**
     * Completes message encoding.
     *
     * @param output the hash
     * @return this encoder
     */
    Encoder complete(ByteBuffer output);

  }

}
