/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.provider;

import com.netradius.cryptolib.BCHelper;
import com.netradius.cryptolib.CryptoException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;

class RSAProviderImpl implements RSAProvider {

  private static void init(Cipher cipher, int mode, KeyPair keyPair, SecureRandom secureRandom)
      throws CryptoException {
    BCHelper.register();
    try {
      if (secureRandom == null) {
        cipher.init(mode, mode == Cipher.ENCRYPT_MODE ? keyPair.getPublic() : keyPair.getPrivate());
      } else {
        cipher.init(mode, mode == Cipher.ENCRYPT_MODE ? keyPair.getPublic() : keyPair.getPrivate(), secureRandom);
      }
    } catch (InvalidKeyException x) {
      throw new CryptoException("Failed to initialize cipher: " + x.getMessage(), x);
    }
  }

  private static KeyPair keyPair(int keySize, SecureRandom secureRandom)
      throws CryptoException {
    try {
      KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
      if (secureRandom == null) {
        keyPairGenerator.initialize(keySize);
      } else {
        keyPairGenerator.initialize(keySize, secureRandom);
      }
      return keyPairGenerator.generateKeyPair();
    } catch (NoSuchAlgorithmException x) {
      throw new CryptoException("Failed to generate RSA key pair: " + x.getMessage(), x);
    }
  }

  static class RSABuilderImpl implements RSABuilder {

    private int keySize;
    private String transform;
    private SecureRandom secureRandom;
    private KeyPair keyPair;

    RSABuilderImpl(int keySize) {
      this.keySize = keySize;
    }

    RSABuilderImpl(KeyPair keyPair) {
      this.keyPair = keyPair;
    }

    @Override
    public RSABuilder random(SecureRandom secureRandom) {
      this.secureRandom = secureRandom;
      return this;
    }

    @Override
    public RSABuilder transform(String transform) {
      this.transform = transform;
      return this;
    }

    @Override
    public RSA rsa() throws CryptoException {
      if (transform == null) {
        BCHelper.register();
      }
      return new RSAImpl(
          keyPair != null ? keyPair : keyPair(keySize, secureRandom),
          transform != null ? transform : DEFAULT_TRANSFORM,
          secureRandom);
    }
  }

  static class RSAImpl implements RSA {

    private KeyPair keyPair;
    private SecureRandom secureRandom;
    private String transform;

    RSAImpl(KeyPair keyPair, String transform, SecureRandom secureRandom)  {
      this.keyPair = keyPair;
      this.transform = transform;
      this.secureRandom = secureRandom;
    }

    @Override
    public KeyPair keyPair() {
      return keyPair;
    }

    @Override
    public Cipher cipher() throws CryptoException {
      try {
        return Cipher.getInstance(transform);
      } catch (NoSuchAlgorithmException | NoSuchPaddingException x) {
        throw new CryptoException("Failed to create cipher: " + x.getMessage(), x);
      }
    }

    @Override
    public byte[] encrypt(byte[] plaintext) throws CryptoException {
      Cipher cipher = cipher();
      init(cipher, Cipher.ENCRYPT_MODE, keyPair, secureRandom);
      try {
        return cipher.doFinal(plaintext);
      } catch (IllegalBlockSizeException | BadPaddingException x) {
        throw new CryptoException("Failed to encrypt data: " + x.getMessage(), x);
      }
    }

    @Override
    public byte[] decrypt(byte[] ciphertext) throws CryptoException {
      Cipher cipher = cipher();
      init(cipher, Cipher.DECRYPT_MODE, keyPair, secureRandom);
      try {
        return cipher.doFinal(ciphertext);
      } catch (IllegalBlockSizeException | BadPaddingException x) {
        throw new CryptoException("Failed to decrypt data: " + x.getMessage(), x);
      }
    }
  }

}
