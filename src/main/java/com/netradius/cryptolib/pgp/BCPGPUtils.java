/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.pgp;

import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;
import org.bouncycastle.bcpg.sig.Features;
import org.bouncycastle.bcpg.sig.KeyFlags;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.openpgp.*;
import org.bouncycastle.openpgp.operator.PBESecretKeyEncryptor;
import org.bouncycastle.openpgp.operator.PGPDigestCalculator;
import org.bouncycastle.openpgp.operator.bc.*;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.Iterator;

/*
 * A lot of this functionality was built referencing examples from
 * https://github.com/bcgit/bc-java/tree/master/pg/src/main/java/org/bouncycastle/openpgp
 */

/**
 * Bouncy castle utility methods. This class is not intended to be consumed directly and is used by PGPHelper.
 *
 * @author Kevin Hawkins
 * @author Erik R. Jensen
 */
public class BCPGPUtils {

	/**
	 * Creates a new PGPKeyRingGenerator.
	 *
	 * @param id the identity
	 * @param pass the password or phrase
	 * @return a new PGPKeyRinGenerator
	 * @throws PGPException if an error occurs
	 */
	public static PGPKeyRingGenerator generateKeyRingGenerator(String id, char[] pass)
			throws PGPException {
		return generateKeyRingGenerator(id, pass, 0xc0);
	}

	/**
	 * Creates a new PGPKeyRingGenerator
	 * @param id the identity
	 * @param pass the password or phrase
	 * @param s2kcount iteration count to use for S2K function.
	 * @return a new PGPKeyRinGenerator
	 * @throws PGPException if an error occurs
	 */
	public static PGPKeyRingGenerator generateKeyRingGenerator(String id, char[] pass, int s2kcount)
			throws PGPException {
		// This object generates individual key-pairs.
		RSAKeyPairGenerator kpg = new RSAKeyPairGenerator();

		// Boilerplate RSA parameters, no need to change anything
		// except for the RSA key-size (2048). You can use whatever
		// key-size makes sense for you -- 4096, etc.
		kpg.init(new RSAKeyGenerationParameters(
				BigInteger.valueOf(0x10001), new SecureRandom(), 2048, 12));

		// First create the master (signing) key with the generator.
		PGPKeyPair rsakp_sign = new BcPGPKeyPair(
				PGPPublicKey.RSA_SIGN, kpg.generateKeyPair(), new Date());

		// Then an encryption subkey.
		PGPKeyPair rsakp_enc = new BcPGPKeyPair(
				PGPPublicKey.RSA_ENCRYPT, kpg.generateKeyPair(), new Date());

		// Add a self-signature on the id
		PGPSignatureSubpacketGenerator signhashgen =
				new PGPSignatureSubpacketGenerator();

		// Add signed metadata on the signature.

		// 1) Declare its purpose
		signhashgen.setKeyFlags(false, KeyFlags.SIGN_DATA|KeyFlags.CERTIFY_OTHER);

		// 2) Set preferences for secondary crypto algorithms to use when sending messages to this key.
		signhashgen.setPreferredSymmetricAlgorithms(false,
				new int[] {
						SymmetricKeyAlgorithmTags.AES_256,
						SymmetricKeyAlgorithmTags.AES_192,
						SymmetricKeyAlgorithmTags.AES_128
				});
		signhashgen.setPreferredHashAlgorithms(false,
				new int[] {
						HashAlgorithmTags.SHA256,
						HashAlgorithmTags.SHA1,
						HashAlgorithmTags.SHA384,
						HashAlgorithmTags.SHA512,
						HashAlgorithmTags.SHA224,
				});
		// 3) Request senders add additional checksums to the message (useful when verifying unsigned messages.)
		signhashgen.setFeature(false, Features.FEATURE_MODIFICATION_DETECTION);

		// Create a signature on the encryption subkey.
		PGPSignatureSubpacketGenerator enchashgen = new PGPSignatureSubpacketGenerator();

		// Add metadata to declare its purpose
		enchashgen.setKeyFlags(false, KeyFlags.ENCRYPT_COMMS|KeyFlags.ENCRYPT_STORAGE);

		// Objects used to encrypt the secret key.
		PGPDigestCalculator sha1Calc = new BcPGPDigestCalculatorProvider().get(HashAlgorithmTags.SHA1);
		PGPDigestCalculator sha256Calc = new BcPGPDigestCalculatorProvider().get(HashAlgorithmTags.SHA256);

		// bcpg 1.48 exposes this API that includes s2kcount. Earlier versions use a default of 0x60.
		PBESecretKeyEncryptor pske = (new BcPBESecretKeyEncryptorBuilder(
				PGPEncryptedData.AES_256, sha256Calc, s2kcount)).build(pass);

		// Finally, create the keyring itself. The constructor
		// takes parameters that allow it to generate the self
		// signature.
		PGPKeyRingGenerator keyRingGen = new PGPKeyRingGenerator(
				PGPSignature.POSITIVE_CERTIFICATION, rsakp_sign, id, sha1Calc, signhashgen.generate(), null,
				new BcPGPContentSignerBuilder(rsakp_sign.getPublicKey().getAlgorithm(), HashAlgorithmTags.SHA1), pske);

		// Add our encryption subkey, together with its signature.
		keyRingGen.addSubKey(rsakp_enc, enchashgen.generate(), null);
		return keyRingGen;
	}

	/**
	 * Reads a secret key from a file.
	 *
	 * @param in the secret key
	 * @return the secret key
	 * @throws IOException if an I/O error occurs
	 * @throws PGPException if a PGP error occurs
	 * @throws IllegalArgumentException if the secret key could not be found
	 */
	public static PGPSecretKey readSecretKey(InputStream in) throws IOException, PGPException {
		PGPSecretKeyRingCollection keyRingCollection = new PGPSecretKeyRingCollection(
				PGPUtil.getDecoderStream(in), new JcaKeyFingerprintCalculator());
		Iterator keyRingIter = keyRingCollection.getKeyRings();
		while (keyRingIter.hasNext()) {
			PGPSecretKeyRing secretKeyRing = (PGPSecretKeyRing)keyRingIter.next();
			Iterator keyIter = secretKeyRing.getSecretKeys();
			while (keyIter.hasNext()) {
				PGPSecretKey secretKey = (PGPSecretKey)keyIter.next();
				if (secretKey.isSigningKey()) {
					return secretKey;
				}
			}
		}
		throw new IllegalArgumentException("Can't find secret key in key ring");
	}

	/**
	 * Reads the public key from the collection.
	 *
	 * @param in the key ring collection
	 * @return the public key
	 * @throws IOException if an I/O error occurs
	 * @throws PGPException if a PGP error occurs
	 * @throws IllegalArgumentException if the public key could not be found
	 */
	public static PGPPublicKey readPublicKeyFromCol(InputStream in) throws IOException, PGPException {
		in = PGPUtil.getDecoderStream(in);
		PGPPublicKeyRingCollection pgpPub = new PGPPublicKeyRingCollection(in, new BcKeyFingerprintCalculator());
		PGPPublicKey key = null;
		Iterator rIt = pgpPub.getKeyRings();
		while (key == null && rIt.hasNext()) {
			PGPPublicKeyRing kRing = (PGPPublicKeyRing) rIt.next();
			Iterator kIt = kRing.getPublicKeys();
			while (key == null && kIt.hasNext()) {
				PGPPublicKey k = (PGPPublicKey) kIt.next();
				if (k.isEncryptionKey()) {
					key = k;
				}
			}
		}
		if (key == null) {
			throw new IllegalArgumentException("Can't find public key in key ring.");
		}
		return key;
	}

	/**
	 * Reads the secret key from the collection.
	 *
	 * @param in the key ring collection
	 * @param keyId the key ID
	 * @return the secret key
	 * @throws IOException if an I/O error occurs
	 * @throws PGPException if a PGP error occurs
	 * @throws IllegalArgumentException if the secret key could not be found
	 */
	public static PGPSecretKey readSecretKeyFromCol(InputStream in, long keyId) throws IOException, PGPException {
		in = PGPUtil.getDecoderStream(in);
		PGPSecretKeyRingCollection pgpSec = new PGPSecretKeyRingCollection(in, new BcKeyFingerprintCalculator());

		PGPSecretKey key = pgpSec.getSecretKey(keyId);

		if (key == null) {
			throw new IllegalArgumentException("Can't find encryption key in key ring.");
		}
		return key;
	}

}
