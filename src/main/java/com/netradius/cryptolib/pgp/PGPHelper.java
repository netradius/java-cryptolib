/**
 * Copyright (c) 2010-2020, NetRadius, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *   3. Neither the name of NetRadius, LLC nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.netradius.cryptolib.pgp;

import com.netradius.commons.io.IOHelper;
import com.netradius.cryptolib.CryptoException;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.BCPGOutputStream;
import org.bouncycastle.bcpg.CompressionAlgorithmTags;
import org.bouncycastle.openpgp.*;
import org.bouncycastle.openpgp.bc.BcPGPObjectFactory;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.bouncycastle.openpgp.operator.bc.*;

import java.io.*;
import java.security.SecureRandom;
import java.util.Iterator;

/*
 * A lot of this functionality was built referencing examples from
 * https://github.com/bcgit/bc-java/tree/master/pg/src/main/java/org/bouncycastle/openpgp
 */

/**
 * PGP related helper functions.
 *
 * @author Kevin Hawkins
 * @author Erik R. Jensen
 */
public class PGPHelper {

	public static final SecureRandom RAND = new SecureRandom();

	/**
	 * Generates a new PGP key pair.
	 *
	 * @param identity the key identity
	 * @param pass the password or phrase
	 * @param armor true to armor the file, false otherwise
	 * @param pubout the stream to write the public key to
	 * @param secout the stream to write the secret key to
	 * @throws CryptoException if an error occurs
	 */
	public static void generate(String identity, String pass, boolean armor,
			OutputStream pubout, OutputStream secout) throws CryptoException {
		try {
			PGPKeyRingGenerator krgen = BCPGPUtils.generateKeyRingGenerator(identity, pass.toCharArray());

			// Generate public key ring, dump to file.
			PGPPublicKeyRing pkr = krgen.generatePublicKeyRing();

			// Generate private key, dump to file.
			PGPSecretKeyRing skr = krgen.generateSecretKeyRing();

			if (armor) {
				pubout = new ArmoredOutputStream(pubout);
				secout = new ArmoredOutputStream(secout);
			}

			pkr.encode(pubout);
			skr.encode(secout);

		} catch (IOException | PGPException x) {
		  throw new CryptoException("Error generating PGP key pair: " + x.getMessage(), x);
    } finally {
			IOHelper.close(pubout);
			IOHelper.close(secout);
		}
	}

	/**
	 * Signs a file using a secret key.
	 *
	 * @param file the file to sign
	 * @param secKeyIn the secret key
	 * @param pass the password or phrase to the secret key
	 * @param armor true to armor the output, false otherwise
	 * @param out the stream to write the signed file to
	 * @throws CryptoException if an error occurs
	 */
	public static void sign(File file, InputStream secKeyIn, String pass, boolean armor, OutputStream out)
		throws CryptoException {
		try {
			if (armor) {
				out = new ArmoredOutputStream(out);
			}

			PGPSecretKey secretKey = BCPGPUtils.readSecretKey(secKeyIn);
			PGPPrivateKey pgpPrivKey = secretKey.extractPrivateKey(new BcPBESecretKeyDecryptorBuilder(
					new BcPGPDigestCalculatorProvider()).build(pass.toCharArray()));
			PGPSignatureGenerator sGen = new PGPSignatureGenerator(new BcPGPContentSignerBuilder(
					secretKey.getPublicKey().getAlgorithm(), PGPUtil.SHA1));
			sGen.init(PGPSignature.BINARY_DOCUMENT, pgpPrivKey);

			Iterator itr = secretKey.getPublicKey().getUserIDs();
			if (itr.hasNext()) {
				PGPSignatureSubpacketGenerator  spGen = new PGPSignatureSubpacketGenerator();
				spGen.setSignerUserID(false, (String)itr.next());
				sGen.setHashedSubpackets(spGen.generate());
			}

			PGPCompressedDataGenerator cGen = new PGPCompressedDataGenerator(PGPCompressedData.ZLIB);

			BCPGOutputStream bOut = new BCPGOutputStream(cGen.open(out));
			sGen.generateOnePassVersion(false).encode(bOut);

			PGPLiteralDataGenerator lGen = new PGPLiteralDataGenerator();
			OutputStream lOut = lGen.open(bOut, PGPLiteralDataGenerator.BINARY, file);

			try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(file))) {
				byte[] buf = new byte[8192];
				int read = in.read(buf);
				while (read != -1) {
					lOut.write(buf, 0, read);
					sGen.update(buf, 0, read);
					read = in.read(buf);
				}
			}

			lGen.close();
			sGen.generate().encode(bOut);
			cGen.close();

		} catch (IOException | PGPException x) {
		  throw new CryptoException("Error signing file: " + x.getMessage(), x);
    } finally {
			IOHelper.close(out);
			IOHelper.close(secKeyIn);
		}
	}

	/**
	 * Verifies a signature on a file.
	 *
	 * @param file the signed file
	 * @param pubKeyIn the public key to use
	 * @param out the stream to write the verified file to
	 * @return true if verified, false if otherwise
	 * @throws CryptoException if an error occurs
	 */
	public static boolean verify(File file, InputStream pubKeyIn, OutputStream out) throws CryptoException {
		try (BufferedInputStream bin = new BufferedInputStream(new FileInputStream(file))) {
			InputStream in = PGPUtil.getDecoderStream(bin);

			BcPGPObjectFactory pgpFact = new BcPGPObjectFactory(in);
			PGPCompressedData c1 = (PGPCompressedData)pgpFact.nextObject();
			pgpFact = new BcPGPObjectFactory(c1.getDataStream());
			PGPOnePassSignatureList p1 = (PGPOnePassSignatureList)pgpFact.nextObject();
			PGPOnePassSignature ops = p1.get(0);

			PGPLiteralData p2 = (PGPLiteralData)pgpFact.nextObject();
			InputStream dIn = p2.getInputStream();

			PGPPublicKeyRingCollection pgpRing = new PGPPublicKeyRingCollection(
					PGPUtil.getDecoderStream(pubKeyIn), new BcKeyFingerprintCalculator());
			PGPPublicKey key = pgpRing.getPublicKey(ops.getKeyID());
			ops.init(new BcPGPContentVerifierBuilderProvider(), key);

			byte[] buf = new byte[8192];
			int read = dIn.read(buf);
			while (read != -1) {
				ops.update(buf, 0, read);
				out.write(buf, 0, read);
				read = dIn.read(buf);
			}
			PGPSignatureList p3 = (PGPSignatureList)pgpFact.nextObject();
			return ops.verify(p3.get(0));
		} catch (IOException | PGPException x) {
		  throw new CryptoException("Error verifying signature: " + x.getMessage(), x);
    } finally {
			IOHelper.close(out);
			IOHelper.close(pubKeyIn);
		}
	}

	/**
	 * Encrypts a file.
	 *
	 * @param file the file to encrypt
	 * @param pubKeyIn the public key to use
	 * @param armor true to armor the output, false otherwise
	 * @param withIntegrityCheck true to include an integrity check, false otherwise
	 * @param out the stream to write the encrypted file to
	 * @throws CryptoException if an error occurs
	 */
	public static void encrypt(File file, InputStream pubKeyIn, boolean armor, boolean withIntegrityCheck, OutputStream out)
			throws CryptoException {
		try {
			PGPPublicKey encKey = BCPGPUtils.readPublicKeyFromCol(pubKeyIn);
			if (armor) {
				out = new ArmoredOutputStream(out);
			}

			BcPGPDataEncryptorBuilder encBld = new BcPGPDataEncryptorBuilder(PGPEncryptedData.CAST5)
					.setWithIntegrityPacket(withIntegrityCheck)
					.setSecureRandom(RAND);

			PGPEncryptedDataGenerator encGen = new PGPEncryptedDataGenerator(encBld);
			encGen.addMethod(new BcPublicKeyKeyEncryptionMethodGenerator(encKey));

			PGPCompressedDataGenerator cGen = new PGPCompressedDataGenerator(CompressionAlgorithmTags.ZIP);

			try (OutputStream eOut = encGen.open(out, new byte[8192]);
				 OutputStream cOut = cGen.open(eOut, new byte[8192])) {
				PGPUtil.writeFileToLiteralData(cOut, PGPLiteralData.BINARY, file);
			}
		} catch (IOException | PGPException x) {
		  throw new CryptoException("Error encrypting file: " + x.getMessage(), x);
    } finally {
			IOHelper.close(pubKeyIn);
			IOHelper.close(out);
		}
	}

	/**
	 * Decrypts a file.
	 *
	 * @param file the file to decrypt
	 * @param pubKeyIn the public key
	 * @param secKeyIn the secret key
	 * @param pass the password or phrase to the secret key
	 * @param out the stream to write the decrypted file to
	 * @throws CryptoException if an error occurs
	 */
	public static void decrypt(File file, InputStream pubKeyIn, InputStream secKeyIn, String pass, OutputStream out)
			throws CryptoException {
		try (FileInputStream fis = new FileInputStream(file);
			 BufferedInputStream bis = new BufferedInputStream(fis);
			 InputStream in = PGPUtil.getDecoderStream(bis)) {

			PGPPublicKey pubKey = BCPGPUtils.readPublicKeyFromCol(pubKeyIn);

			PGPSecretKey secKey = BCPGPUtils.readSecretKeyFromCol(secKeyIn, pubKey.getKeyID());

			PGPPrivateKey privKey = secKey.extractPrivateKey(new BcPBESecretKeyDecryptorBuilder(
					new BcPGPDigestCalculatorProvider()).build(pass.toCharArray()));

			JcaPGPObjectFactory pgpObjectFactory = new JcaPGPObjectFactory(in);

			Object obj = pgpObjectFactory.nextObject();

			//if the first object is a PGP marker packet
			PGPEncryptedDataList enc = obj instanceof  PGPEncryptedDataList
					? (PGPEncryptedDataList)obj
					: (PGPEncryptedDataList)pgpObjectFactory.nextObject();

			Iterator it = enc.getEncryptedDataObjects();
			PGPPublicKeyEncryptedData pbe = (PGPPublicKeyEncryptedData)it.next();

			InputStream clear = pbe.getDataStream(new BcPublicKeyDataDecryptorFactory(privKey));
			JcaPGPObjectFactory plainFact = new JcaPGPObjectFactory(clear);
			Object message = plainFact.nextObject();

			if (message instanceof PGPCompressedData) {
				PGPCompressedData compressedData = (PGPCompressedData)message;
				JcaPGPObjectFactory pgpFact = new JcaPGPObjectFactory(compressedData.getDataStream());
				message = pgpFact.nextObject();
			}
			if (message instanceof PGPLiteralData) {
				PGPLiteralData literalData = (PGPLiteralData)message;
				try (InputStream unc = literalData.getInputStream()) {
					byte[] buf = new byte[8192];
					int read = unc.read(buf);
					while (read != -1) {
						out.write(buf, 0, read);
						read = unc.read(buf);
					}
				}
			} else if (message instanceof PGPOnePassSignatureList) {
				throw new PGPException("Encrypted message contains a signed message - not literal data.");
			} else {
				throw new PGPException("Message is not a simple encrypted file - type unknown");
			}
		} catch (IOException | PGPException x) {
		  throw new CryptoException("Error decrypting file: " + x.getMessage(), x);
    } finally {
			IOHelper.close(pubKeyIn);
			IOHelper.close(secKeyIn);
			IOHelper.close(out);
		}
	}
}
